package io.harc.auth;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;

public class TokenService {
    public static  String createToken(String key,String... ads) {
        String token= JWT.create().withAudience(ads)// 将 user id 保存到 token 里面
                .sign(Algorithm.HMAC256(key));// 以 password 作为 token 的密钥
        return token;
    }
}
