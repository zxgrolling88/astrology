package io.harc.cache.serialize;

import com.esotericsoftware.kryo.DefaultSerializer;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryo.serializers.CompatibleFieldSerializer;
import com.fasterxml.jackson.core.type.TypeReference;

import io.harc.cache.interfaces.ISerialize;
import org.objenesis.strategy.StdInstantiatorStrategy;

import java.io.ByteArrayOutputStream;
import java.util.*;

/**
 * @author: chenliangtaiyi
 * 高性能的序列化Kryo,并且比json序列化简捷,不用传递类型.
 */
public class KryoSerializer implements ISerialize {
	private Kryo kryo = new Kryo();


	public KryoSerializer(){
		super();
		kryo.setDefaultSerializer(CompatibleFieldSerializer.class);
	}
	
	@Override
	public byte[] serialize(Object value) throws  Exception {
		try(ByteArrayOutputStream stream = new ByteArrayOutputStream()) {
			Output output = new Output(stream);
			this.kryo.writeClassAndObject(output, value);
			output.flush();
			output.close();
			byte[] bs= stream.toByteArray();
			return bs;
		}

//		byte[] buffer = new byte[2048];
//		Output output = new Output(buffer);
//		kryo.writeClassAndObject(output, value);
//		byte[] bs= output.toBytes();
//		output.close();
//		return bs;
	}

	@Override
	public <T> T deserialize(byte[] bytes) throws  Exception {
		Input input = new Input(bytes);
		//System.out.println("deserialize");
		@SuppressWarnings("unchecked")
		T t = (T) kryo.readClassAndObject(input);
		input.close();
		return t;
	}

	@Override
	public <T> T deserialize(byte[] bytes, Class<T> claz) throws Exception {
		// TODO Auto-generated method stub
		Input input = new Input(bytes);
		T t =kryo.readObject(input,claz);
		input.close();
		return t;
	}

	@Override
	public <T> T deserialize(byte[] bytes, TypeReference tf) throws Exception { 
		return deserialize(bytes);
	}

}
