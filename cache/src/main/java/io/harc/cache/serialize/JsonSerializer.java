package io.harc.cache.serialize;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
 
import io.harc.cache.interfaces.ISerialize;

/**
 * @author: chenliangtaiyi
 * json 序列化
 * 如果跨平台请用此序列化.
 */
public class JsonSerializer implements ISerialize {

	private static ObjectMapper mapper = new ObjectMapper();

	static {
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
	}
	

	@Override
	public    byte[] serialize(Object value) throws Exception{ 
		return mapper.writeValueAsBytes(value); 
	} 
	


	@Override
	public <T> T deserialize(byte[] bytes, Class<T> claz) throws Exception {
		return (T)mapper.readValue(bytes, claz);
	}
	
 
	@Override
	public <T>  T deserialize(byte[] bytes, TypeReference tf) throws Exception {
		return (T)mapper.readValue(bytes, tf);
	}
  
	@Override
	public <T>  T deserialize(byte[] bytes) throws Exception {
		return (T)mapper.readValue(bytes, String.class);
//		String s=(new String(bytes));  
//		return (T)s; 
		////Log.write(LogAction.Error, "JsonSerializer", "deserialize", "cache", 0,"Json 不支持不带类型的反序列化");
		
		//throw new Exception("Json 不支持不带类型的反序列化");
	}
 
 

}
