package io.harc.cache.serialize;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryo.serializers.CompatibleFieldSerializer;
import com.fasterxml.jackson.core.type.TypeReference;
import io.harc.cache.interfaces.ISerialize;
import org.nustaq.serialization.FSTConfiguration;

import java.io.ByteArrayOutputStream;

/**
 * @author: chenliangtaiyi
 * 高性能的序列化Kryo,并且比json序列化简捷,不用传递类型.
 */
public class FstSerializer implements ISerialize {
	static FSTConfiguration conf = FSTConfiguration
			 .createDefaultConfiguration();
			//.createStructConfiguration();
	
	@Override
	public byte[] serialize(Object value) throws  Exception {
		return conf.asByteArray(value);
	}

	@Override
	public <T> T deserialize(byte[] bytes) throws  Exception {
		return (T)conf.asObject(bytes);
	}

	@Override
	public <T> T deserialize(byte[] bytes, Class<T> claz) throws Exception {
		return deserialize(bytes);
	}

	@Override
	public <T> T deserialize(byte[] bytes, TypeReference tf) throws Exception { 
		return deserialize(bytes);
	}

}
