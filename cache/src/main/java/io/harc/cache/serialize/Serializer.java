package io.harc.cache.serialize;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import com.fasterxml.jackson.core.type.TypeReference;

import io.harc.cache.interfaces.ISerialize;

/**
 * @author: chenliangtaiyi
 * JAVA序列化
 *
 */
public class Serializer  implements ISerialize {

	public    byte[] serialize(Object object) {
		ObjectOutputStream oos = null;
		ByteArrayOutputStream baos = null;
		try {
			// 序列化
			baos = new ByteArrayOutputStream();
			oos = new ObjectOutputStream(baos);
			oos.writeObject(object);
			oos.flush();
			byte[] bytes = baos.toByteArray();
			return bytes;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (baos != null) {
					baos.close();
				}
				if (oos != null) {
					oos.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	@Override
	public <T> T deserialize(byte[] bytes) {
		ByteArrayInputStream bais = null;
		try {
			// 反序列化
			bais = new ByteArrayInputStream((byte[])bytes);
			ObjectInputStream ois = new ObjectInputStream(bais);
			return (T)ois.readObject();
		} catch (Exception e) {

		} finally {
			try {
				if (bais != null) {
					bais.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public <T> T deserialize(byte[] bytes, Class<T> claz) throws Exception {
		// TODO Auto-generated method stub
		return deserialize(bytes);
	}

	@Override
	public <T> T deserialize(byte[] bytes, TypeReference tf) throws Exception {
		// TODO Auto-generated method stub
		return deserialize(bytes);
	}
 
 
}
