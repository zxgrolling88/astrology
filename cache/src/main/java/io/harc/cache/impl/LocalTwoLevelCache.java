package io.harc.cache.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import io.harc.cache.interfaces.ICache;

import java.util.Date;

/**
 * @author chenliangtaiyi
 * 二级缓存,如果内存缓存不存在,就自动从远程缓存取.
 */
public class LocalTwoLevelCache implements ICache {

    LocalCache  local = null;
    ICache remote;

    public LocalTwoLevelCache(String localCacheSpec,ICache remote){
        try {

            System.out.println("LocalTwoLevelCache:localCacheSpec= "+localCacheSpec);
            this.remote=remote;
            local =new LocalCache(localCacheSpec);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }



    @Override
    public <T> T get(String key)   {
        try {
            T t=(T)local.get(key);
            if(null==t)
                t=remote.get(key);
            return t;

        }
        catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public <T> T get(String key, Class<T> claz)
    {
        try {
            System.out.println("LocalTwoLevelCache.get:"+key);
            T t=(T)local.get(key);
            if(null==t)
                t=remote.get(key,claz);
            return t;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public <T> T get(String key, TypeReference tf) {
        try {
            T t=(T)local.get(key);
            if(null==t)
                t=remote.get(key,tf);
            return t;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public <T> String set(String key, T obj) {
        String res="Fail";
        try {
            local.set(key,obj);
            remote.set(key,obj);
            res="Ok";
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return res;

    }

    @Override
    public <T> String set(String key, T obj, int seconds) {
        String res="Fail";
        try {
            local.set(key,obj);
            remote.set(key,obj,seconds);
            res="Ok";
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public <T> String set(String key, T obj, Date expDate) {
        String res="Fail";
        Date now = new Date();
        try {
            long seconds = (expDate.getTime() - now.getTime())/1000;
            local.set(key,obj);
            remote.set(key,obj,(int)seconds);
            res="Ok";
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public   long remove(String... keys) {
        long rtn=-1;
        try {
            System.out.println("LocalTwoLevelCache.开始清除 "+keys.length+" 条缓存");
            local.remove( keys);
            rtn=remote.remove(keys);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return rtn;
    }

}
