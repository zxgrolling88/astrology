package io.harc.cache.impl;

import io.harc.cache.config.CodisConfigure;
import redis.clients.jedis.Jedis;

/**
 * @author: chenliangtaiyi
 * 支持codis连接
 */
public class CodisCache extends RedisCache  {

    private CodisPool jedisPool;
    public CodisCache(String cacheApplication){
    	super(cacheApplication);
    }
    
    @Override
    protected void initPool(String cacheApplication){

        long t = System.currentTimeMillis();
        configuration = new CodisConfigure(cacheApplication);
        jedisPool = new CodisPool(cacheApplication);
        //Log.write(LogAction.Set, this.getClass().getName(), "initPool", "cache", System.currentTimeMillis()-t,"cacheApplication:"+cacheApplication );

    }
    @Override
    public Jedis getJedis(){
        return jedisPool.getPool();
    }

}
