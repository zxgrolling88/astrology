package io.harc.cache.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import io.harc.cache.interfaces.ICache;

import java.util.Date;

public class LocalCache  implements ICache {

    Cache cache = null;
    public LocalCache(String localCacheSpec){
        try {

            System.out.println("LocalTwoLevelCache:localCacheSpec= "+localCacheSpec);
            cache = CacheBuilder.from(localCacheSpec).build();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public <T> T get(String key)   {
        try {
            return (T)cache.getIfPresent(key);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public <T> T get(String key, Class<T> claz)
    {
        try {
            return (T)cache.getIfPresent(key);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public <T> T get(String key, TypeReference tf) {
        try {
            return (T)cache.getIfPresent(key);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public <T> String set(String key, T obj) {
        String res="Fail";
        try {
            cache.put(key,obj);
            res="Ok";
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return res;

    }

    @Override
    public <T> String set(String key, T obj, int seconds) {
        String res="Fail";
        try {
            cache.put(key,obj);
            res="Ok";
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public <T> String set(String key, T obj, Date expDate) {
        String res="Fail";
        Date now = new Date();
        try {
            long seconds = expDate.getTime() - now.getTime();
            //Cache<String, T> cache = CacheBuilder.newBuilder().expireAfterWrite(seconds, TimeUnit.SECONDS).build();
            cache.put(key,obj);
            res="Ok";
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public   long remove(String... keys) {
        long rtn=-1;
        try {
            for(String key : keys)
                cache.invalidate(key);
            rtn=0;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return rtn;
    }

}
