package io.harc.cache.impl;

import io.harc.cache.config.RedisConfiguration;
import io.harc.common.StringUtils;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.JedisSentinelPool;

import java.util.HashSet;
import java.util.Set;

/**
 * @author: chenliangtaiyi
 * 默认的Redis缓存支持
 */

@SuppressWarnings("unchecked")
public class RedisSentineCache  extends RedisCache {

	//Logger logger=  LoggerFactory.getLogger(RedisSentineCache.class) ;

	public RedisSentineCache(){

	}


	private RedisConfiguration conf;

	JedisSentinelPool jsPool = null;


	@Override
	public Jedis getJedis(){
		if(null==this.jsPool)
			System.out.println(" RedisSentineCache.getJedis.jsPool 为空! 当前实例:"+this.hashCode());
		return this.jsPool.getResource();
	}

	public RedisSentineCache(String cacheApplication) {
		initPool(cacheApplication);
		initSerializer();
//		super(cacheApplication);
	}

	@Override
	protected void initPool(String cacheApplication){
		long t = System.currentTimeMillis();
		try{
			configuration = new RedisConfiguration(cacheApplication);
			conf=(RedisConfiguration)configuration;
			final String master=conf.getMaster();
			final String host = conf.getRedisHost();
			final int port=conf.getRedisPort();
			Set<String> hosts=new HashSet<String>();
			String[] hs=host.split(",");
			for(String s:hs){
				hosts.add(s+":"+port);
			}
			final String auth = StringUtils.isBlank(conf.getRedisAuth()) ? null : conf.getRedisAuth();
			final int redisPoolMaxActives = conf.getRedisPoolMaxActive();
			final int redisPoolTimeout = conf.getRedisPoolTimeout();
			final int redisPoolMaxIdle = conf.getRedisPoolMaxIdle();
			JedisPoolConfig poolConfig = new JedisPoolConfig();
			poolConfig.setMaxWaitMillis(redisPoolTimeout);
			poolConfig.setMaxIdle(redisPoolMaxIdle);
			poolConfig.setMaxTotal(redisPoolMaxActives); 
			if(null==auth || auth.trim().length()==0)
				this.jsPool=new JedisSentinelPool( master, hosts,poolConfig ,redisPoolTimeout);
			else
				this.jsPool = new JedisSentinelPool(master, hosts,poolConfig ,redisPoolTimeout,auth);
			if(null==this.jsPool)
				System.out.println(" RedisSentineCache.POOL 初始化失败为空!");
			System.out.println(" RedisSentineCache.配置信息:" + conf);
			//logger.info(String.format(Constants.LOG_FORMAT, this.getClass().getName(), System.currentTimeMillis() - t,
			//		"initPool", " RedisSentineCache.配置信息:" + conf));
		}catch(Exception e){
//			logger.info(String.format(Constants.LOG_FORMAT, this.getClass().getName(), System.currentTimeMillis() - t,
//					"RedisSentineCache.initPool", "错误:" + e.getMessage()));
			e.printStackTrace();
		}

		//Log.write(LogAction.Error, this.getClass().getName(), "initPool", "cache", System.currentTimeMillis()-t,e );


	}

}
