package io.harc.cache.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import io.harc.cache.config.RedisClusterConfiguration;
import io.harc.cache.interfaces.ICache;
import io.harc.cache.interfaces.IRedis;
import io.harc.cache.interfaces.ISerialize;
import io.harc.common.StringUtils;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;

import java.util.*;

/**
 * @author: chenliangtaiyi
 * 默认的Redis缓存支持
 */

@SuppressWarnings("unchecked")
public class RedisClusterCache implements ICache, IRedis {

    protected static final String CHARSET = "UTF-8";
    protected ISerialize serializer;

    public RedisClusterCache() {

    } 
    private JedisCluster cluster ;
    private RedisClusterConfiguration conf;

    public RedisClusterCache(String cacheApplication) {
        long t = System.currentTimeMillis();
        try {
            initPool(cacheApplication);
            initSerializer();
        } catch (Exception e) {
            //Log.write(LogAction.Error, this.getClass().getName(), "RedisCache", "cache", System.currentTimeMillis()-t ,e);
            e.printStackTrace();
        }
    }

    protected void initPool(String cacheApplication) {
        long t = System.currentTimeMillis();
        try {

            conf = new RedisClusterConfiguration(cacheApplication);
            //retrieve nodes
            Set<HostAndPort> nodeSet = new HashSet<>();
            String nodes = conf.getNodes();
            if (null != nodes && nodes.length() > 0) {
                try {
                    String ss[] = nodes.split(",");
                    String hp[];
                    for (String s : ss) {
                        hp = s.split(":");
                        nodeSet.add(new HostAndPort(hp[0], StringUtils.parseInteger(hp[1])));

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            GenericObjectPoolConfig poolConfig = new GenericObjectPoolConfig();
            poolConfig.setMaxIdle(conf.getRedisPoolMaxIdle());
            poolConfig.setMaxTotal(conf.getRedisPoolMaxTotal());
            poolConfig.setMaxWaitMillis(conf.getRedisMaxWaitMillis());
            poolConfig.setTestOnBorrow(conf.isTestOnBorrow());
            int maxAttempts = conf.getMaxAttempts();
            int timeout = conf.getRedisPoolTimeout();
            String password = conf.getRedisAuth();
            System.out.println("RedisClusterCache.initPool:nodeSet="+nodeSet+ " maxAttempts="+maxAttempts +" conf.getRedisPoolMaxIdle()="+conf.getRedisPoolMaxIdle() +" conf.getRedisPoolMaxTotal()="+conf.getRedisPoolMaxTotal() +" conf.getRedisMaxWaitMillis():"+conf.getRedisMaxWaitMillis());
            if ("".equals(password)) {
                  cluster = new JedisCluster(nodeSet, timeout, timeout, maxAttempts, poolConfig);
            } else {
                System.out.println("RedisClusterCache.initPool:nodeSet="+nodeSet +" password:"+password);
                cluster = new JedisCluster(nodeSet, timeout, timeout, maxAttempts, password, poolConfig);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void initSerializer() {
        long t = System.currentTimeMillis();
        final String serializerName = conf.getSerializer().toLowerCase();
        serializer=SerializerUtil.instance(serializerName) ;
    }


    @Override
    public <T> T get(String key) {
        long t = System.currentTimeMillis();
        try {
            byte[] obj = cluster.get(key.getBytes(CHARSET));
            if (obj != null) {
                T result = serializer.deserialize(obj);
                //Log.write(LogAction.Get, this.getClass().getName(), "get", "cache", System.currentTimeMillis()-t ,"缓存命中key: "+key);
                return result;
            } else {
                //Log.write(LogAction.Get, this.getClass().getName(), "get", "cache", System.currentTimeMillis()-t ,"缓存没命中key: "+key);
            }

        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "get", "cache", System.currentTimeMillis()-t ,e);
        } finally {
            close();
        }
        return null;
    }

    private void close(){
//        if (cluster != null) {
//            try {
//                cluster.close();
//            }
//            catch (Exception e){e.printStackTrace();}
//        }
    }

    @Override
    public <T> T get(String key, Class<T> claz) {
        long t = System.currentTimeMillis();
        try {
            byte[] obj = cluster.get(key.getBytes(CHARSET));
            if (obj != null) {
                T result = serializer.deserialize(obj, claz);
                //Log.write(LogAction.Get, this.getClass().getName(), "get", "cache", System.currentTimeMillis()-t ,"缓存命中key: "+key);
                return result;
            } else {
                //Log.write(LogAction.Get, this.getClass().getName(), "get", "cache", System.currentTimeMillis()-t ,"缓存没命中key: "+key);
            }

        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "get", "cache", System.currentTimeMillis()-t ,e);
        } finally {
            close();
        }
        return null;
    }


    @Override
    public <T> T get(String key, TypeReference tf) {
        long t = System.currentTimeMillis();
        ////Log.write(LogAction.Get, this.getClass().getName(), "get", "cache", System.currentTimeMillis()-t ,"开始获取缓存: "+key);

        try {
            byte[] obj = cluster.get(key.getBytes(CHARSET));
            if (obj != null) {
                //Log.write(LogAction.Get, this.getClass().getName(), "get", "cache", System.currentTimeMillis()-t ,"缓存命中key: "+key);
                T result = serializer.deserialize(obj, tf);
                return result;
            } else {
                //Log.write(LogAction.Get, this.getClass().getName(), "get", "cache", System.currentTimeMillis()-t ,"缓存没命中key: "+key);
            }
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "get", "cache", System.currentTimeMillis()-t ,e);
        } finally {
            close();
        }
        return null;
    }


    @Override
    public <T> String set(String key, T obj) {
        String res = "Fail";
        long t = System.currentTimeMillis();
        try {
            byte[] bytes = serializer.serialize(obj);
            res = cluster.set(key.getBytes(CHARSET), bytes);
            //Log.write(LogAction.Set, this.getClass().getName(), "set", "cache", System.currentTimeMillis()-t ,"key: "+key +" bytes.length: "+bytes.length);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "set", "cache", System.currentTimeMillis()-t ,e);
            System.out.println("failed in set:"+e.getMessage());
        } finally {
            close();
        }
        return res;
    }


    @Override
    public <T> String set(String key, T obj, int seconds) { 
        String res = "Fail";
        long t = System.currentTimeMillis();
        try {
            byte[] bytes = serializer.serialize(obj); 
            if (seconds <= 0) {
                res = cluster.set(key.getBytes(CHARSET), bytes);
            } else {
                res = cluster.setex(key.getBytes(CHARSET), seconds, bytes);
            }
            //Log.write(LogAction.Set, this.getClass().getName(), "set", "cache", System.currentTimeMillis()-t ,"key: "+key +" bytes.length: "+bytes.length+ " seconds: "+seconds);

        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "set", "cache", System.currentTimeMillis()-t ,e);
        } finally {
            close();
        }
        return res;

    }

    @Override
    public <T> long setnx(String key, T obj) {
        
        long res = 0;
        long t = System.currentTimeMillis();
        try {
            byte[] bytes = serializer.serialize(obj);
            
            res = cluster.setnx(key.getBytes(CHARSET), bytes);
            //Log.write(LogAction.Set, this.getClass().getName(), "set", "cache", System.currentTimeMillis()-t ,"key: "+key +" bytes.length: "+bytes.length+ " seconds: "+seconds);

        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "set", "cache", System.currentTimeMillis()-t ,e);
        } finally {
            close();
        }
        return res;

    }

    @Override
    public <T> String set(String key, T obj, Date expDate) {
        
        Date now = new Date();
        String res = "Fail";
        long t = now.getTime();
        try {
            if (expDate.before(now)) {
                return "设置过期日期要大于当前日期.";
            }
            Long seconds = expDate.getTime() - now.getTime();
            byte[] bytes = serializer.serialize(obj);
            
            res = cluster.setex(key.getBytes(CHARSET), seconds.intValue(), bytes);
            //Log.write(LogAction.Set, this.getClass().getName(), "set", "cache", System.currentTimeMillis()-t ,"key: "+key +" bytes.length: "+bytes.length+ " seconds: "+seconds);

        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "set", "cache", System.currentTimeMillis()-t ,e);
        } finally {
            close();
        }
        return res;
    }


    @Override
    public <T> T getSet(String key, T obj) {
        
        try {
            byte[] bytes = serializer.serialize(obj);
            
            byte[] result = cluster.getSet(key.getBytes(CHARSET), bytes);
            return serializer.deserialize(result);
            //Log.write(LogAction.Set, this.getClass().getName(), "set", "cache", System.currentTimeMillis()-t ,"key: "+key +" bytes.length: "+bytes.length+ " seconds: "+seconds);

        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "set", "cache", System.currentTimeMillis()-t ,e);
        } finally {
            close();
        }
        return null;
    }


    @Override
    public long expire(String key, int seconds) {
        
        long t = System.currentTimeMillis();
        long rtn = 0;
        try {
            
            rtn = cluster.expire(key, seconds);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Set, this.getClass().getName(), "expire", "cache", System.currentTimeMillis()-t ,e);
        } finally {
            close();
        }
        return rtn;

    }


    @Override
    public long srem(String key, String... members) {
        
        long t = System.currentTimeMillis();
        long rtn = 0;
        try {
            
            cluster.srem(key, members);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Set, this.getClass().getName(), "expire", "cache", System.currentTimeMillis()-t ,e);
        } finally {
            close();
        }
        return rtn;

    }


    @Override
    public long remove(String... keys) {
        //long t = System.currentTimeMillis();
        long n = 0;
        try {
            n= cluster.del(keys);
            System.out.println("RedisCache.清除" + n + "条缓存  keys:" + String.join(",", keys));

        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Set, this.getClass().getName(), "remove", "cache", System.currentTimeMillis()-t ,e);
        } finally {
            close();
        }
        return n;

    }

    @Override
    public Set<String> keys(String pattern) {
        //long t = System.currentTimeMillis();
        try {
            
            return cluster.hkeys(pattern);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "keys", "cache", System.currentTimeMillis()-t ,e);
            return null;
        } finally {
            close();
        }
    }

    @Override
    public Long rpush(String key, String... strings) {
        
        long t = System.currentTimeMillis();
        try {
            
            return cluster.rpush(key, strings);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "keys", "cache", System.currentTimeMillis()-t ,e);
            return null;
        } finally {
            close();
        }
    }

    @Override
    public Long lpush(String key, String... strings) {
        
        long t = System.currentTimeMillis();
        try {
            
            return cluster.lpush(key, strings);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "keys", "cache", System.currentTimeMillis()-t ,e);
            return null;
        } finally {
            close();
        }
    }

    @Override
    public Long llen(String key) {
        
        long t = System.currentTimeMillis();
        try {
            
            return cluster.llen(key);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "keys", "cache", System.currentTimeMillis()-t ,e);
            return null;
        } finally {
            close();
        }
    }

    @Override
    public List<String> lrange(String key, long start, long end) {
        
        long t = System.currentTimeMillis();
        try {
            
            return cluster.lrange(key, start, end);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "keys", "cache", System.currentTimeMillis()-t ,e);
            return null;
        } finally {
            close();
        }
    }

    @Override
    public String ltrim(String key, long start, long end) {
        
        long t = System.currentTimeMillis();
        try {
            
            return cluster.ltrim(key, start, end);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "keys", "cache", System.currentTimeMillis()-t ,e);
            return null;
        } finally {
            close();
        }
    }

    @Override
    public String lindex(String key, long index) {
        
        long t = System.currentTimeMillis();
        try {
            
            return cluster.lindex(key, index);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "keys", "cache", System.currentTimeMillis()-t ,e);
            return null;
        } finally {
            close();
        }
    }

    @Override
    public String lset(String key, long index, String value) {
        
        long t = System.currentTimeMillis();
        try {
            
            return cluster.lset(key, index, value);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "keys", "cache", System.currentTimeMillis()-t ,e);
            return null;
        } finally {
            close();
        }
    }

    @Override
    public Long lrem(String key, long count, String value) {
        
        long t = System.currentTimeMillis();
        try {
            
            return cluster.lrem(key, count, value);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "keys", "cache", System.currentTimeMillis()-t ,e);
            return null;
        } finally {
            close();
        }
    }

    @Override
    public String lpop(String key) {
        
        long t = System.currentTimeMillis();
        try {
            
            return cluster.lpop(key);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "keys", "cache", System.currentTimeMillis()-t ,e);
            return null;
        } finally {
            close();
        }
    }

    @Override
    public String rpop(String key) {
        
        long t = System.currentTimeMillis();
        try {
            
            return cluster.rpop(key);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "keys", "cache", System.currentTimeMillis()-t ,e);
            return null;
        } finally {
            close();
        }
    }

    @Override
    public String rpoplpush(String srckey, String dstkey) {
        
        long t = System.currentTimeMillis();
        try {
            
            return cluster.rpoplpush(srckey, dstkey);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "keys", "cache", System.currentTimeMillis()-t ,e);
            return null;
        } finally {
            close();
        }
    }

    /**
     * @param key
     * @param count
     * @return
     */
    @Override
    public List<String> lpopTrans(String key, long count) {
        
          return  null;
    }

    @Override
    public List<Object> lpopPipelined(String key, long count) {
         return  null;
    }


    @Override
    public long incrBy(String key, long step) {
        
        long t = System.currentTimeMillis();
        try {
            
            return cluster.incrBy(key.getBytes(CHARSET), step);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "incrBy", "cache", System.currentTimeMillis()-t ,e);
            return -1;
        } finally {
            close();
        }
    }

    @Override
    public long decrBy(String key, long step) {
        
        long t = System.currentTimeMillis();
        try {
            
            return cluster.decrBy(key.getBytes(CHARSET), step);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "decrBy", "cache", System.currentTimeMillis()-t ,e);
            return -1;
        } finally {
            close();
        }
    }


    @Override
    public List<Object> watch(String key, String value) {
         return  null;
    }

    @Override
    public long setnx(String key, String value) {
        
        long t = System.currentTimeMillis();
        long rtn = -1;
        try {
            
            rtn = cluster.setnx(key, value);
            //Log.write(LogAction.Get, this.getClass().getName(), "setnx", "redis", System.currentTimeMillis()-t ,key);

        } catch (Exception e) {
            e.printStackTrace();
            rtn = -1;
            //Log.write(LogAction.Error, this.getClass().getName(), "setnx", "redis", System.currentTimeMillis()-t ,key+e.getMessage());
        } finally {
            close();
        }
        return rtn;
    }


    @Override
    public long sadd(String key, String... members) {
        
        long t = System.currentTimeMillis();
        long rtn = -1;
        try {
            
            rtn = cluster.sadd(key, members);
            //Log.write(LogAction.Get, this.getClass().getName(), "sadd", "redis", System.currentTimeMillis()-t ,key);

        } catch (Exception e) {
            e.printStackTrace();
            rtn = -1;
            //Log.write(LogAction.Error, this.getClass().getName(), "sadd", "redis", System.currentTimeMillis()-t ,key+e.getMessage());
        } finally {
            close();
        }
        return rtn;
    }


    @Override
    public Set<String> smembers(String key) {
        
        long t = System.currentTimeMillis();
        Set<String> set = null;
        try {
            
            set = cluster.smembers(key);
            //Log.write(LogAction.Get, this.getClass().getName(), "smembers", "redis", System.currentTimeMillis()-t ,key);

        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "smembers", "redis", System.currentTimeMillis()-t ,key+e.getMessage());
        } finally {
            close();
        }
        return set;
    }


    @Override
    public Set<String> spop(String key, long count) {
        
        long t = System.currentTimeMillis();
        Set<String> set = null;
        try {
            
            set = cluster.spop(key, count);
            //Log.write(LogAction.Get, this.getClass().getName(), "spop", "redis", System.currentTimeMillis()-t ,key);

        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "spop", "redis", System.currentTimeMillis()-t ,key+e.getMessage());
        } finally {
            close();
        }
        return set;
    }


    @Override
    public long del(String... keys) {
        
        long t = System.currentTimeMillis();
        long rtn = 0;
        try {
            
            rtn = cluster.del(keys);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Set, this.getClass().getName(), "del", "cache", System.currentTimeMillis()-t ,e);

        } finally {
            close();
        }
        return rtn;
    }

    @Override
    public long hset(String key, String member, String value) {
        
        long t = System.currentTimeMillis();
        long rtn = 0;
        try {
            
            rtn = cluster.hset(key, member, value);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "hset", "redis", System.currentTimeMillis()-t ,e);
        } finally {
            close();
        }
        return rtn;
    }

    @Override
    public <T> long hset(String key, String member, T value) {
        
        long t = System.currentTimeMillis();
        long rtn = 0;
        try {
            byte[] keybytes = serializer.serialize(key);
            byte[] membytes = serializer.serialize(member);
            byte[] valuebytes = serializer.serialize(value);
            
            rtn = cluster.hset(keybytes, membytes, valuebytes);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "hset", "redis", System.currentTimeMillis()-t ,e);
        } finally {
            close();
        }
        return rtn;
    }

    @Override
    public Long hsetnx(final String key, final String field, final String value) {
        
        long t = System.currentTimeMillis();
        long rtn = 0;
        try {
            
            rtn = cluster.hsetnx(key, field, value);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "hset", "redis", System.currentTimeMillis()-t ,e);
        } finally {
            close();
        }
        return rtn;
    }

    @Override
    public Map<String, String> hgetAll(String key) {
        
        long t = System.currentTimeMillis();
        Map<String, String> map = Maps.newHashMap();
        try {
            
            map = cluster.hgetAll(key);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "hgetAll", "redis", System.currentTimeMillis()-t ,e);
        } finally {
            close();
        }
        return map;
    }

    @Override
    public List<String> hvals(String key) {
        
        long t = System.currentTimeMillis();
        List<String> list = null;
        try {
            
            list = cluster.hvals(key);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "hVals", "redis", System.currentTimeMillis()-t ,e);
        } finally {
            close();
        }
        return list;
    }

    @Override
    public String hget(String key, String member) {
        
        long t = System.currentTimeMillis();
        String s = "";
        try {
            
            s = cluster.hget(key, member);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "hget", "redis", System.currentTimeMillis()-t ,e);
        } finally {
            close();
        }
        return s;
    }

    @Override
    public <T> T hget(String key, String member, Class<T> claz) {
        
        long t = System.currentTimeMillis();
        T s = null;
        try {
            byte[] obj= cluster.hget(key.getBytes(), member.getBytes());

            if ( null != obj) {
                T result  = serializer.deserialize(obj,claz);
                //Log.write(LogAction.Get, this.getClass().getName(), "hget", "redis", System.currentTimeMillis()-t ,"缓存命中key_field: "+key+"_"+member);
                return result;
            } else {
                //Log.write(LogAction.Get, this.getClass().getName(), "hget", "redis", System.currentTimeMillis()-t ,"缓存没命中key_field: "+key+"_"+member);
            }
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "hget", "redis", System.currentTimeMillis()-t ,e);
        } finally {
            close();
        }
        return s;
    }

    @Override
    public boolean hexists(String key, String member) {
        
        long t = System.currentTimeMillis();
        boolean b = false;
        try {
            
            b = cluster.hexists(key, member);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "hexists", "redis", System.currentTimeMillis()-t ,e);
        } finally {
            close();
        }
        return b;
    }

    @Override
    public long hdel(String key, String... members) {
        
        long t = System.currentTimeMillis();
        long l = 0;
        try {
            
            l = cluster.hdel(key, members);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "hdel", "redis", System.currentTimeMillis()-t ,e);
        } finally {
            close();
        }
        return l;
    }

    @Override
    public Long hincrBy(final String key, final String field, final long value) {
        
        long t = System.currentTimeMillis();
        long l = 0;
        try {
            
            byte [] keyByte = serializer.serialize(key);
            byte [] fieldByte = serializer.serialize(field);
            l = cluster.hincrBy(keyByte, fieldByte, value);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "hdel", "redis", System.currentTimeMillis()-t ,e);
        } finally {
            close();
        }
        return l;
    }
    @Override
    public <T> Map<String, T> hgetAll(String key, Class<T> claz) {
        Map<String, T> ret = Maps.newHashMap();
        Map<String, String> temp = hgetAll(key);
        for (String member : ret.keySet()) {
            String val = temp.get(member);
            if (!StringUtils.isEmpty(val)) {
                long time = 0;
                try {
                    time = System.currentTimeMillis();
                    T t = serializer.deserialize(val.getBytes(CHARSET), claz);
                    ret.put(member, t);
                } catch (Exception e) {
                    e.printStackTrace();
                    //Log.write(LogAction.Error, this.getClass().getName(), "hgetAll", "redis", System.currentTimeMillis() - time, e);
                }
            }
        }

        return ret;
    }

    @Override
    public <T> List<T> havls(String key, Class<T> claz) {
        List<T> ret = Lists.newArrayList();
        List<String> vals = hvals(key);
        for (String val : vals) {
            if (!StringUtils.isEmpty(val)) {
                long time = 0;
                try {
                    time = System.currentTimeMillis();
                    T t = serializer.deserialize(val.getBytes(CHARSET), claz);
                    ret.add(t);
                } catch (Exception e) {
                    e.printStackTrace();
                    //Log.write(LogAction.Error, this.getClass().getName(), "havls", "redis", System.currentTimeMillis() - time, e);
                }
            }
        }
        return ret;
    }

    public long lock(String key, String value, int seconds) {
        try {
            long rtn = 0;
            rtn = setnx(key, value);
            if (rtn > 0) expire(key, seconds);
            return rtn;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    public long unlock(String... keys) {
        try {
            long rtn = 0;
            rtn = del(keys);
            return rtn;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }


}
