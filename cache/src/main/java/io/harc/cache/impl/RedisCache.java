package io.harc.cache.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import io.harc.cache.config.Configure;
import io.harc.cache.config.RedisConfiguration;
import io.harc.cache.interfaces.ICache;
import io.harc.cache.interfaces.IRedis;
import io.harc.cache.interfaces.ISerialize;
import io.harc.common.StringUtils;
import redis.clients.jedis.*;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author: chenliangtaiyi
 * 默认的Redis缓存支持
 */

@SuppressWarnings("unchecked")
public class RedisCache implements ICache, IRedis {

    protected static final String CHARSET = "UTF-8";
    protected Configure configuration;
    protected ISerialize serializer;

    public RedisCache() {

    }


    private JedisPool POOL;
    private RedisConfiguration conf;

    public Jedis getJedis() {
        return POOL.getResource();
    }

    public RedisCache(String cacheApplication) {
        long t = System.currentTimeMillis();
        try {
            initPool(cacheApplication);
            initSerializer();
        } catch (Exception e) {
            //Log.write(LogAction.Error, this.getClass().getName(), "RedisCache", "cache", System.currentTimeMillis()-t ,e);
            e.printStackTrace();
        }
    }

    protected void initPool(String cacheApplication) {
        long t = System.currentTimeMillis();
        try {
            configuration = new RedisConfiguration(cacheApplication);
            conf = (RedisConfiguration) configuration;
            final String host = conf.getRedisHost();
            final int port = conf.getRedisPort();
            final String auth = StringUtils.isBlank(conf.getRedisAuth()) ? null : conf.getRedisAuth();
            final int redisPoolMaxActives = conf.getRedisPoolMaxActive();
            final int redisPoolTimeout = conf.getRedisPoolTimeout();
            final int redisPoolMaxIdle = conf.getRedisPoolMaxIdle();
            JedisPoolConfig poolConfig = new JedisPoolConfig();
            poolConfig.setMaxWaitMillis(redisPoolTimeout);
            poolConfig.setMaxIdle(redisPoolMaxIdle);
            poolConfig.setMaxTotal(redisPoolMaxActives);
            System.out.println("RedisCache.cacheApplication:" + cacheApplication + " redis.host:" + host + " redis.port:" + port);
            if (null == auth || auth.trim().length() == 0)
                this.POOL = new JedisPool(poolConfig, host, port, redisPoolTimeout);
            else
                this.POOL = new JedisPool(poolConfig, host, port, redisPoolTimeout, auth);
            //Log.write(LogAction.Set, this.getClass().getName(), "initPool", "cache", System.currentTimeMillis()-t,"cacheApplication:"+cacheApplication +" " +conf.getRedisHost());
        } catch (Exception e) {
//    		 Log.write(LogAction.Error, this.getClass().getName(), "initPool", "cache", System.currentTimeMillis()-t,e );
            e.printStackTrace();
        }

        //Log.write(LogAction.Error, this.getClass().getName(), "initPool", "cache", System.currentTimeMillis()-t,e );


    }


    protected void initSerializer() {
        long t = System.currentTimeMillis();
        final String serializerName = configuration.getSerializer().toLowerCase();
        serializer=SerializerUtil.instance(serializerName) ;
    }


    @Override
    public <T> T get(String key) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        try {
            jedis = getJedis();
            byte[] obj = jedis.get(key.getBytes(CHARSET));
            if (obj != null) {
                T result = serializer.deserialize(obj);
                //Log.write(LogAction.Get, this.getClass().getName(), "get", "cache", System.currentTimeMillis()-t ,"缓存命中key: "+key);
                return result;
            } else {
                //Log.write(LogAction.Get, this.getClass().getName(), "get", "cache", System.currentTimeMillis()-t ,"缓存没命中key: "+key);
            }

        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "get", "cache", System.currentTimeMillis()-t ,e);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return null;
    }


    @Override
    public <T> T get(String key, Class<T> claz) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        try {
            jedis = getJedis();
            byte[] obj = jedis.get(key.getBytes(CHARSET));
            if (obj != null) {
                T result = serializer.deserialize(obj, claz);
                //Log.write(LogAction.Get, this.getClass().getName(), "get", "cache", System.currentTimeMillis()-t ,"缓存命中key: "+key);
                return result;
            } else {
                //Log.write(LogAction.Get, this.getClass().getName(), "get", "cache", System.currentTimeMillis()-t ,"缓存没命中key: "+key);
            }

        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "get", "cache", System.currentTimeMillis()-t ,e);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return null;
    }


    @Override
    public <T> T get(String key, TypeReference tf) {
        long t = System.currentTimeMillis();
        ////Log.write(LogAction.Get, this.getClass().getName(), "get", "cache", System.currentTimeMillis()-t ,"开始获取缓存: "+key);
        Jedis jedis = null;
        try {
            jedis = getJedis();
            byte[] obj = jedis.get(key.getBytes(CHARSET));
            if (obj != null) {
                //Log.write(LogAction.Get, this.getClass().getName(), "get", "cache", System.currentTimeMillis()-t ,"缓存命中key: "+key);
                T result = serializer.deserialize(obj, tf);
                return result;
            } else {
                //Log.write(LogAction.Get, this.getClass().getName(), "get", "cache", System.currentTimeMillis()-t ,"缓存没命中key: "+key);
            }
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "get", "cache", System.currentTimeMillis()-t ,e);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return null;
    }


    @Override
    public <T> String set(String key, T obj) {
        Jedis jedis = null;
        String res = "Fail";
        long t = System.currentTimeMillis();
        try {
            byte[] bytes = serializer.serialize(obj);
            jedis = getJedis();
            res = jedis.set(key.getBytes(CHARSET), bytes);
            //Log.write(LogAction.Set, this.getClass().getName(), "set", "cache", System.currentTimeMillis()-t ,"key: "+key +" bytes.length: "+bytes.length);
        } catch (Exception e) {
            //Log.write(LogAction.Error, this.getClass().getName(), "set", "cache", System.currentTimeMillis()-t ,e);
            System.out.println("failed in set");
            e.printStackTrace();
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return res;
    }


    @Override
    public <T> String set(String key, T obj, int seconds) {
        Jedis jedis = null;
        String res = "Fail";
        long t = System.currentTimeMillis();
        try {
            byte[] bytes = serializer.serialize(obj);
            jedis = getJedis();
            if (seconds <= 0) {
                res = jedis.set(key.getBytes(CHARSET), bytes);
            } else {
                res = jedis.setex(key.getBytes(CHARSET), seconds, bytes);
            }
            //Log.write(LogAction.Set, this.getClass().getName(), "set", "cache", System.currentTimeMillis()-t ,"key: "+key +" bytes.length: "+bytes.length+ " seconds: "+seconds);

        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "set", "cache", System.currentTimeMillis()-t ,e);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return res;

    }

    @Override
    public <T> long setnx(String key, T obj) {
        Jedis jedis = null;
        long res = 0;
        long t = System.currentTimeMillis();
        try {
            byte[] bytes = serializer.serialize(obj);
            jedis = getJedis();
            res = jedis.setnx(key.getBytes(CHARSET), bytes);
            //Log.write(LogAction.Set, this.getClass().getName(), "set", "cache", System.currentTimeMillis()-t ,"key: "+key +" bytes.length: "+bytes.length+ " seconds: "+seconds);

        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "set", "cache", System.currentTimeMillis()-t ,e);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return res;

    }

    @Override
    public <T> String set(String key, T obj, Date expDate) {
        Jedis jedis = null;
        Date now = new Date();
        String res = "Fail";
        long t = now.getTime();
        try {
            if (expDate.before(now)) {
                return "设置过期日期要大于当前日期.";
            }
            Long seconds = expDate.getTime() - now.getTime();
            byte[] bytes = serializer.serialize(obj);
            jedis = getJedis();
            res = jedis.setex(key.getBytes(CHARSET), seconds.intValue(), bytes);
            //Log.write(LogAction.Set, this.getClass().getName(), "set", "cache", System.currentTimeMillis()-t ,"key: "+key +" bytes.length: "+bytes.length+ " seconds: "+seconds);

        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "set", "cache", System.currentTimeMillis()-t ,e);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return res;
    }


    @Override
    public <T> T getSet(String key, T obj) {
        Jedis jedis = null;
        try {
            byte[] bytes = serializer.serialize(obj);
            jedis = getJedis();
            byte[] result = jedis.getSet(key.getBytes(CHARSET), bytes);
            return serializer.deserialize(result);
            //Log.write(LogAction.Set, this.getClass().getName(), "set", "cache", System.currentTimeMillis()-t ,"key: "+key +" bytes.length: "+bytes.length+ " seconds: "+seconds);

        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "set", "cache", System.currentTimeMillis()-t ,e);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return null;
    }


    @Override
    public long expire(String key, int seconds) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        long rtn = 0;
        try {
            jedis = getJedis();
            rtn = jedis.expire(key, seconds);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Set, this.getClass().getName(), "expire", "cache", System.currentTimeMillis()-t ,e);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return rtn;

    }


    @Override
    public long srem(String key, String... members) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        long rtn = 0;
        try {
            jedis = getJedis();
            jedis.srem(key, members);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Set, this.getClass().getName(), "expire", "cache", System.currentTimeMillis()-t ,e);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return rtn;

    }


    @Override
    public long remove(String... keys) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        long n = 0;
        Pipeline pipe = null;
        try {
            jedis = getJedis();
            pipe = jedis.pipelined();
            Response<Long> res = pipe.del(keys);
            pipe.sync();
            n = res.get();
            System.out.println("RedisCache.清除" + n + "条缓存  keys:" + String.join(",", keys));
            // n=jedis.del(keys);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Set, this.getClass().getName(), "remove", "cache", System.currentTimeMillis()-t ,e);
        } finally {
            if (null != pipe)
                try {
                    pipe.close();
                } catch (Exception e) {
                }
            if (jedis != null) {
                jedis.close();
            }
        }
        return n;

    }

    @Override
    public Set<String> keys(String pattern) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        try {
            jedis = getJedis();
            return jedis.keys(pattern);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "keys", "cache", System.currentTimeMillis()-t ,e);
            return null;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    @Override
    public Long rpush(String key, String... strings) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        try {
            jedis = getJedis();
            return jedis.rpush(key, strings);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "keys", "cache", System.currentTimeMillis()-t ,e);
            return null;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    @Override
    public Long lpush(String key, String... strings) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        try {
            jedis = getJedis();
            return jedis.lpush(key, strings);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "keys", "cache", System.currentTimeMillis()-t ,e);
            return null;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    @Override
    public Long llen(String key) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        try {
            jedis = getJedis();
            return jedis.llen(key);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "keys", "cache", System.currentTimeMillis()-t ,e);
            return null;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    @Override
    public List<String> lrange(String key, long start, long end) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        try {
            jedis = getJedis();
            return jedis.lrange(key, start, end);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "keys", "cache", System.currentTimeMillis()-t ,e);
            return null;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    @Override
    public String ltrim(String key, long start, long end) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        try {
            jedis = getJedis();
            return jedis.ltrim(key, start, end);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "keys", "cache", System.currentTimeMillis()-t ,e);
            return null;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    @Override
    public String lindex(String key, long index) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        try {
            jedis = getJedis();
            return jedis.lindex(key, index);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "keys", "cache", System.currentTimeMillis()-t ,e);
            return null;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    @Override
    public String lset(String key, long index, String value) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        try {
            jedis = getJedis();
            return jedis.lset(key, index, value);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "keys", "cache", System.currentTimeMillis()-t ,e);
            return null;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    @Override
    public Long lrem(String key, long count, String value) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        try {
            jedis = getJedis();
            return jedis.lrem(key, count, value);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "keys", "cache", System.currentTimeMillis()-t ,e);
            return null;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    @Override
    public String lpop(String key) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        try {
            jedis = getJedis();
            return jedis.lpop(key);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "keys", "cache", System.currentTimeMillis()-t ,e);
            return null;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    @Override
    public String rpop(String key) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        try {
            jedis = getJedis();
            return jedis.rpop(key);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "keys", "cache", System.currentTimeMillis()-t ,e);
            return null;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    @Override
    public String rpoplpush(String srckey, String dstkey) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        try {
            jedis = getJedis();
            return jedis.rpoplpush(srckey, dstkey);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "keys", "cache", System.currentTimeMillis()-t ,e);
            return null;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    /**
     * @param key
     * @param count
     * @return
     */
    @Override
    public List<String> lpopTrans(String key, long count) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        try {
            jedis = getJedis();
            Transaction tx = jedis.multi();

            /*不能获取长度，确认是否有问题
            Response<Long> llenResponse = tx.llen(key);

            long llen = llenResponse.get();
            // 修正count
            if (count > llenResponse.)
                count = llen;
            */
            tx.lrange(key, 0, count);
            tx.ltrim(key, count, -1);

            List<Object> results = tx.exec();

            return (List<String>) results.get(0);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "keys", "cache", System.currentTimeMillis()-t ,e);
            return null;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    @Override
    public List<Object> lpopPipelined(String key, long count) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        try {
            jedis = getJedis();
            Pipeline pipeline = jedis.pipelined();
            for (int i = 0; i < count; i++) {
                pipeline.lpop(key);
            }

            List<Object> results = pipeline.syncAndReturnAll();
            long end = System.currentTimeMillis();

            return results;
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "keys", "cache", System.currentTimeMillis()-t ,e);
            return null;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }


    @Override
    public long incrBy(String key, long step) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        try {
            jedis = getJedis();
            return jedis.incrBy(key.getBytes(CHARSET), step);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "incrBy", "cache", System.currentTimeMillis()-t ,e);
            return -1;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }

    @Override
    public long decrBy(String key, long step) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        try {
            jedis = getJedis();
            return jedis.decrBy(key.getBytes(CHARSET), step);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "decrBy", "cache", System.currentTimeMillis()-t ,e);
            return -1;
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
    }


    @Override
    public List<Object> watch(String key, String value) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        List<Object> result = null;
        try {
            jedis = getJedis();
            jedis.watch(key);
            Transaction transaction = jedis.multi();
            transaction.set(key, String.valueOf(t));
            result = transaction.exec();
            //Log.write(LogAction.Get, this.getClass().getName(), "watch", "redis", System.currentTimeMillis()-t ,key);

        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "watch", "redis", System.currentTimeMillis()-t ,key+e.getMessage());
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return result;
    }

    @Override
    public long setnx(String key, String value) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        long rtn = -1;
        try {
            jedis = getJedis();
            rtn = jedis.setnx(key, value);
            //Log.write(LogAction.Get, this.getClass().getName(), "setnx", "redis", System.currentTimeMillis()-t ,key);

        } catch (Exception e) {
            e.printStackTrace();
            rtn = -1;
            //Log.write(LogAction.Error, this.getClass().getName(), "setnx", "redis", System.currentTimeMillis()-t ,key+e.getMessage());
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return rtn;
    }


    @Override
    public long sadd(String key, String... members) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        long rtn = -1;
        try {
            jedis = getJedis();
            rtn = jedis.sadd(key, members);
            //Log.write(LogAction.Get, this.getClass().getName(), "sadd", "redis", System.currentTimeMillis()-t ,key);

        } catch (Exception e) {
            e.printStackTrace();
            rtn = -1;
            //Log.write(LogAction.Error, this.getClass().getName(), "sadd", "redis", System.currentTimeMillis()-t ,key+e.getMessage());
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return rtn;
    }


    @Override
    public Set<String> smembers(String key) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        Set<String> set = null;
        try {
            jedis = getJedis();
            set = jedis.smembers(key);
            //Log.write(LogAction.Get, this.getClass().getName(), "smembers", "redis", System.currentTimeMillis()-t ,key);

        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "smembers", "redis", System.currentTimeMillis()-t ,key+e.getMessage());
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return set;
    }


    @Override
    public Set<String> spop(String key, long count) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        Set<String> set = null;
        try {
            jedis = getJedis();
            set = jedis.spop(key, count);
            //Log.write(LogAction.Get, this.getClass().getName(), "spop", "redis", System.currentTimeMillis()-t ,key);

        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "spop", "redis", System.currentTimeMillis()-t ,key+e.getMessage());
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return set;
    }


    @Override
    public long del(String... keys) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        long rtn = 0;
        try {
            jedis = getJedis();
            rtn = jedis.del(keys);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Set, this.getClass().getName(), "del", "cache", System.currentTimeMillis()-t ,e);

        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return rtn;
    }

    @Override
    public long hset(String key, String member, String value) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        long rtn = 0;
        try {
            jedis = getJedis();
            rtn = jedis.hset(key, member, value);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "hset", "redis", System.currentTimeMillis()-t ,e);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return rtn;
    }

    @Override
    public <T> long hset(String key, String member, T value) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        long rtn = 0;
        try {
            byte[] keybytes = serializer.serialize(key);
            byte[] membytes = serializer.serialize(member);
            byte[] valuebytes = serializer.serialize(value);
            jedis = getJedis();
            rtn = jedis.hset(keybytes, membytes, valuebytes);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "hset", "redis", System.currentTimeMillis()-t ,e);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return rtn;
    }

    @Override
    public Long hsetnx(final String key, final String field, final String value) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        long rtn = 0;
        try {
            jedis = getJedis();
            rtn = jedis.hsetnx(key, field, value);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "hset", "redis", System.currentTimeMillis()-t ,e);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return rtn;
    }

    @Override
    public Map<String, String> hgetAll(String key) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        Map<String, String> map = Maps.newHashMap();
        try {
            jedis = getJedis();
            map = jedis.hgetAll(key);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "hgetAll", "redis", System.currentTimeMillis()-t ,e);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return map;
    }

    @Override
    public List<String> hvals(String key) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        List<String> list = null;
        try {
            jedis = getJedis();
            list = jedis.hvals(key);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "hVals", "redis", System.currentTimeMillis()-t ,e);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return list;
    }

    @Override
    public String hget(String key, String member) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        String s = "";
        try {
            jedis = getJedis();
            s = jedis.hget(key, member);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "hget", "redis", System.currentTimeMillis()-t ,e);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return s;
    }

    @Override
    public <T> T hget(String key, String member, Class<T> claz) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        T s = null;
        try {
            jedis = getJedis();
            byte[] obj = jedis.hget(key.getBytes(), member.getBytes());

            if ( null != obj) {
                T result  = serializer.deserialize(obj,claz);
                //Log.write(LogAction.Get, this.getClass().getName(), "hget", "redis", System.currentTimeMillis()-t ,"缓存命中key_field: "+key+"_"+member);
                return result;
            } else {
                //Log.write(LogAction.Get, this.getClass().getName(), "hget", "redis", System.currentTimeMillis()-t ,"缓存没命中key_field: "+key+"_"+member);
            }
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "hget", "redis", System.currentTimeMillis()-t ,e);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return s;
    }

    @Override
    public boolean hexists(String key, String member) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        boolean b = false;
        try {
            jedis = getJedis();
            b = jedis.hexists(key, member);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "hexists", "redis", System.currentTimeMillis()-t ,e);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return b;
    }

    @Override
    public long hdel(String key, String... members) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        long l = 0;
        try {
            jedis = getJedis();
            l = jedis.hdel(key, members);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "hdel", "redis", System.currentTimeMillis()-t ,e);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return l;
    }

    @Override
    public Long hincrBy(final String key, final String field, final long value) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        long l = 0;
        try {
            jedis = getJedis();
            byte [] keyByte = serializer.serialize(key);
            byte [] fieldByte = serializer.serialize(field);
            l = jedis.hincrBy(keyByte, fieldByte, value);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "hdel", "redis", System.currentTimeMillis()-t ,e);
        } finally {
            if (jedis != null) {
                jedis.close();
            }
        }
        return l;
    }
    @Override
    public <T> Map<String, T> hgetAll(String key, Class<T> claz) {
        Map<String, T> ret = Maps.newHashMap();
        Map<String, String> temp = hgetAll(key);
        for (String member : ret.keySet()) {
            String val = temp.get(member);
            if (!StringUtils.isEmpty(val)) {
                long time = 0;
                try {
                    time = System.currentTimeMillis();
                    T t = serializer.deserialize(val.getBytes(CHARSET), claz);
                    ret.put(member, t);
                } catch (Exception e) {
                    e.printStackTrace();
                    //Log.write(LogAction.Error, this.getClass().getName(), "hgetAll", "redis", System.currentTimeMillis() - time, e);
                }
            }
        }

        return ret;
    }

    @Override
    public <T> List<T> havls(String key, Class<T> claz) {
        List<T> ret = Lists.newArrayList();
        List<String> vals = hvals(key);
        for (String val : vals) {
            if (!StringUtils.isEmpty(val)) {
                long time = 0;
                try {
                    time = System.currentTimeMillis();
                    T t = serializer.deserialize(val.getBytes(CHARSET), claz);
                    ret.add(t);
                } catch (Exception e) {
                    e.printStackTrace();
                    //Log.write(LogAction.Error, this.getClass().getName(), "havls", "redis", System.currentTimeMillis() - time, e);
                }
            }
        }
        return ret;
    }

    public long lock(String key, String value, int seconds) {
        try {
            long rtn = 0;
            rtn = setnx(key, value);
            if (rtn > 0) expire(key, seconds);
            return rtn;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    public long unlock(String... keys) {
        try {
            long rtn = 0;
            rtn = del(keys);
            return rtn;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }


}
