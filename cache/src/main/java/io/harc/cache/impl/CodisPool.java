package io.harc.cache.impl;

import io.codis.jodis.JedisResourcePool;
import io.codis.jodis.RoundRobinJedisPool;
import io.harc.cache.config.CodisConfigure;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPoolConfig;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: chenliangtaiyi
 * codis连接池
 */

public class CodisPool {

	private static Map<String, JedisResourcePool> jedisPools = new HashMap<String, JedisResourcePool>();
	private CodisConfigure cf;

	public CodisPool(String cacheApplication) {
		cf = new CodisConfigure(cacheApplication);
		if (!jedisPools.containsKey(cacheApplication)) {
			JedisPoolConfig poolConfig = new JedisPoolConfig();
			// 最大空闲连接数, 应用自己评估
			poolConfig.setMaxIdle(cf.getCodisMaxidle());
			// 最大连接数, 应用自己评估
			poolConfig.setMaxTotal(cf.getCodisMaxtotal());
			poolConfig.setTestOnBorrow(false);
			poolConfig.setTestOnReturn(false);
			JedisResourcePool jedisPool = RoundRobinJedisPool.create().curatorClient(cf.getZkAddr(), 30000)
					.zkProxyDir(cf.getZkDir()).password(cf.getCodisPwd()).poolConfig(poolConfig).build();
			jedisPools.put(cacheApplication, jedisPool);
			destroyJedisPool(jedisPool);
		}
	}

	public Jedis getPool() {
		try {
			return jedisPools.get(cf.getCacheApplication()).getResource();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * when closing your application
	 */
	public void destroyJedisPool(final JedisResourcePool jedisPool) {
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				try {
					if (jedisPool != null) {
						jedisPool.close();
						System.out.println("shutdown jedispool");
					}
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		});
	}

}
