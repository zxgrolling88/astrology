package io.harc.cache.impl;


import io.harc.cache.interfaces.ISerialize;
import io.harc.cache.serialize.FstSerializer;
import io.harc.cache.serialize.JsonSerializer;
import io.harc.cache.serialize.KryoSerializer;
import io.harc.cache.serialize.Serializer;

public class SerializerUtil {

    protected static ISerialize instance(String serializerName) {
        ISerialize serializer;
        long t = System.currentTimeMillis();
        switch (serializerName) {
            case "fst":
                serializer = new FstSerializer();
                break;
            case "json":
                serializer = new JsonSerializer();
                break;
            case "kryo":
                serializer = new KryoSerializer();
                break;
            default:
                serializer = new Serializer();
                break;
        }
        return serializer;
        //Log.write(LogAction.Set, this.getClass().getName(), "initSerializer", "cache", System.currentTimeMillis()-t,"serializerName:"+serializerName );

    }
}
