package io.harc.cache.config;

import io.harc.common.Config;

/**
 * @author: chenliangtaiyi
 * codis配置
 */
public class CodisConfigure  extends Configure{

	protected  String zkAddr;
	protected  String zkDir;
	protected  String codisPwd;
	protected  int codisMaxidle;
	protected  int codisMaxtotal;
	protected  int localcacheSize;


	public CodisConfigure(String cacheApplication) {
		init(cacheApplication);
	}

	@Override
	protected  void init(String cacheApplication){
		this.zkAddr=Config.get(cacheApplication,"zk.address","127.0.0.1:2181");
		this.zkDir=Config.get(cacheApplication,"zk.dir","/zk/codis/db_codis_cluster/proxy");
		this.codisPwd=Config.get(cacheApplication,"codis.password","123456");
		this.codisMaxidle=Config.get(cacheApplication,"codis.maxidle",200);
		this.codisMaxtotal=Config.get(cacheApplication,"codis.maxtotal",300);
		this.localcacheSize=Config.get(cacheApplication,"localcache.size",500);
		super.init(cacheApplication);
	}


	public String getZkAddr() {
		return zkAddr;
	}

	public void setZkAddr(String zkAddr) {
		this.zkAddr = zkAddr;
	}

	public String getZkDir() {
		return zkDir;
	}

	public void setZkDir(String zkDir) {
		this.zkDir = zkDir;
	}

	public String getCodisPwd() {
		return codisPwd;
	}

	public void setCodisPwd(String codisPwd) {
		this.codisPwd = codisPwd;
	}

	public int getCodisMaxidle() {
		return codisMaxidle;
	}

	public void setCodisMaxidle(int codisMaxidle) {
		this.codisMaxidle = codisMaxidle;
	}

	public int getCodisMaxtotal() {
		return codisMaxtotal;
	}

	public void setCodisMaxtotal(int codisMaxtotal) {
		this.codisMaxtotal = codisMaxtotal;
	}

	public int getLocalcacheSize() {
		return localcacheSize;
	}

	public void setLocalcacheSize(int localcacheSize) {
		this.localcacheSize = localcacheSize;
	}
}
