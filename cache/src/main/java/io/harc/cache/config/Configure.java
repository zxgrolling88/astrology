package io.harc.cache.config;


import io.harc.common.Config;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: chenliangtaiyi
 * 基础配置
 */
public class Configure {
	protected static Map<String, Configure> configs = new HashMap<String, Configure>();

	/**
	 * 是否启动缓存
	 */
	public  int enable =1;//默认启用缓存

	protected  int database=0;

	protected   String cacheApplication;
	protected 	String serializer="kryo";
	protected  void   init(String cacheApplication){
		this.cacheApplication=cacheApplication;
		this.database=Config.get(cacheApplication,"redis.database",0);
		this.serializer=Config.get(cacheApplication,"redis.serialization",serializer);
		this.enable=Config.get(cacheApplication,"enable",enable);
		configs.put(cacheApplication,this);
	}


	public Configure(){

	}

	public int getEnable() {
		return enable;
	}

	public void setEnable(int enable) {
		this.enable = enable;
	}

	public Configure(String cacheApplication) {
		init(cacheApplication);
	}

	public String getCacheApplication() {
		return cacheApplication;
	}

	public void setCacheApplication(String cacheApplication) {
		this.cacheApplication = cacheApplication;
	}


	public String getSerializer() {
		return serializer;
	}

	public void setSerializer(String serializer) {
		this.serializer = serializer;
	}


}
