package io.harc.cache.config;

import io.harc.common.Config;

/**
 * @author: chenliangtaiyi
 * redis配置
 */
public class RedisClusterConfiguration extends RedisConfiguration {

    public RedisClusterConfiguration(String cacheApplication) {
       init(cacheApplication);
    }


    String nodes;

    boolean testOnBorrow;

    int maxAttempts;

    @Override
    protected  void init(String cacheApplication){
        super.init(cacheApplication);
        this.nodes=Config.get(cacheApplication,"redis.cluster.nodes","");
        this.testOnBorrow=Config.get(cacheApplication,"redis.cluster.testOnBorrow",true);
        this.maxAttempts=Config.get(cacheApplication,"redis.cluster.maxAttempts",10);
        System.out.println("RedisClusterConfiguration.init:nodes="+nodes);
    }

    public String getNodes() {
        return nodes;
    }

    public void setNodes(String nodes) {
        this.nodes = nodes;
    }

    public boolean isTestOnBorrow() {
        return testOnBorrow;
    }

    public void setTestOnBorrow(boolean testOnBorrow) {
        this.testOnBorrow = testOnBorrow;
    }

    public int getMaxAttempts() {
        return maxAttempts;
    }

    public void setMaxAttempts(int maxAttempts) {
        this.maxAttempts = maxAttempts;
    }

    @Override
    public String toString() {
        return "RedisClusterConfiguration{" +
                "nodes='" + nodes + '\'' +
                ", redisAuth='" + redisAuth + '\'' +
                ", redisPoolTimeout=" + redisPoolTimeout +
                ", redisPoolMaxActive=" + redisPoolMaxActive +
                ", redisPoolMaxIdle=" + redisPoolMaxIdle +
                '}';
    }
}
