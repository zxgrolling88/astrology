package io.harc.cache.config;


import io.harc.common.Config;

/**
 * @author: chenliangtaiyi
 * redis配置
 */
public class RedisConfiguration extends Configure {

    protected String master;
    protected String redisHost;
    protected int redisPort;
    protected String redisAuth="";
    protected int redisPoolTimeout;
    protected int redisPoolMaxActive;
    protected int redisPoolMaxIdle;
    protected int redisPoolMinIdle;
    protected int redisPoolMaxTotal;
    protected int redisMaxWaitMillis;

    public RedisConfiguration(){}

    public RedisConfiguration(String cacheApplication) {
        init(cacheApplication);
    }

    @Override
    protected  void init(String cacheApplication){
        super.init(cacheApplication);
        this.master=Config.get(cacheApplication,"redis.master","mymaster");
        this.redisHost=Config.get(cacheApplication,"redis.host","127.0.0.1");
        this.redisPort=Config.get(cacheApplication,"redis.port",6379);
        this.redisAuth=Config.get(cacheApplication,"redis.auth","");
        this.redisPoolTimeout=Config.get(cacheApplication,"redis.pool.timeout",2000);
        this.redisPoolMaxActive=Config.get(cacheApplication,"redis.pool.maxActive",100);
        this.redisPoolMaxIdle=Config.get(cacheApplication,"redis.pool.maxIdle",8);
        this.redisPoolMinIdle=Config.get(cacheApplication,"redis.pool.minIdle",1);
        this.redisPoolMaxTotal=Config.get(cacheApplication,"redis.pool.maxTotal",500);
        this.redisMaxWaitMillis=Config.get(cacheApplication,"redis.pool.maxWaitMillis",2000);

    }

    public int getRedisPoolMinIdle() {
        return redisPoolMinIdle;
    }

    public void setRedisPoolMinIdle(int redisPoolMinIdle) {
        this.redisPoolMinIdle = redisPoolMinIdle;
    }

    public int getRedisPoolMaxTotal() {
        return redisPoolMaxTotal;
    }

    public void setRedisPoolMaxTotal(int redisPoolMaxTotal) {
        this.redisPoolMaxTotal = redisPoolMaxTotal;
    }

    public int getRedisMaxWaitMillis() {
        return redisMaxWaitMillis;
    }

    public void setRedisMaxWaitMillis(int redisMaxWaitMillis) {
        this.redisMaxWaitMillis = redisMaxWaitMillis;
    }

    public String getMaster() {
        return master;
    }

    public void setMaster(String master) {
        this.master = master;
    }

    public String getRedisHost() {
        return redisHost;
    }

    public void setRedisHost(String redisHost) {
        this.redisHost = redisHost;
    }

    public int getRedisPort() {
        return redisPort;
    }

    public void setRedisPort(int redisPort) {
        this.redisPort = redisPort;
    }

    public String getRedisAuth() {
        return redisAuth;
    }

    public void setRedisAuth(String redisAuth) {
        this.redisAuth = redisAuth;
    }

    public int getRedisPoolTimeout() {
        return redisPoolTimeout;
    }

    public void setRedisPoolTimeout(int redisPoolTimeout) {
        this.redisPoolTimeout = redisPoolTimeout;
    }

    public int getRedisPoolMaxActive() {
        return redisPoolMaxActive;
    }

    public void setRedisPoolMaxActive(int redisPoolMaxActive) {
        this.redisPoolMaxActive = redisPoolMaxActive;
    }

    public int getRedisPoolMaxIdle() {
        return redisPoolMaxIdle;
    }

    public void setRedisPoolMaxIdle(int redisPoolMaxIdle) {
        this.redisPoolMaxIdle = redisPoolMaxIdle;
    }

    @Override
    public String toString() {
        return "RedisConfiguration{" +
                "master='" + master + '\'' +
                ", redisHost='" + redisHost + '\'' +
                ", redisPort=" + redisPort +
                ", redisAuth='" + redisAuth + '\'' +
                ", redisPoolTimeout=" + redisPoolTimeout +
                ", redisPoolMaxActive=" + redisPoolMaxActive +
                ", redisPoolMaxidle=" + redisPoolMaxIdle +
                '}';
    }
}
