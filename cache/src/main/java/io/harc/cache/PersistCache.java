package io.harc.cache;//package io.harc.cache;
//
//import java.lang.annotation.ElementType;
//import java.lang.annotation.Retention;
//import java.lang.annotation.RetentionPolicy;
//import java.lang.annotation.Target;
//
///**
// * create by zhangjun1 on 2017/11/6
// * 允许自定义持久化缓存的注解
// */
//@Target(ElementType.METHOD)
//@Retention(RetentionPolicy.RUNTIME)
//public @interface PersistCache {
//
//    String value = null;
//
//}
