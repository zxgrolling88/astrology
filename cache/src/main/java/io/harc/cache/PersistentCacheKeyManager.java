package io.harc.cache;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by wanghanbing on 2017/2/9.
 * 支持模糊和精确类型的缓存key管理.
 */
@Component
public class PersistentCacheKeyManager extends CacheKeyManager {

    @Autowired
    CacheConfig config;
    @Override
    public void addKey(Cache cache, String key) {
        // list类型的key，需要放入，item类型的key不需要  CacheKeyType.LIST.getKey() =":list:"
        if (key.indexOf(CacheKeyType.LIST.getKey()) > -1) {
            cache.sadd(CacheConfig.CacheListKeysKey,key);
        }
    }

    @Override
    public int addClearKey(Cache cache, String key) {
        // item类型的key需要精确清除，返回1，list类型的key需要模糊匹配后批量清除
        if (key.indexOf(CacheKeyType.LIST.getKey()) > -1) {
            cache.sadd(CacheConfig.CacheClearKeysKey, key);//把要清除的Key存缓存
            return 0;
        } else {
            return 1;
        }
    }

}
