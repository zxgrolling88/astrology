package io.harc.cache;

import com.fasterxml.jackson.core.type.TypeReference;
import io.harc.cache.impl.*;
import io.harc.cache.interfaces.ICache;
import io.harc.cache.interfaces.IRedis;
import io.harc.common.Config;
import io.harc.common.Config;
import redis.clients.jedis.Jedis;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author wanghaibing  modify: chenliangtaiyi
 * 支持zk的codis,及普通的redis
 * 序列化默认走Kryo
 * cache.configName: 指定cache的配置文件名(XXX.properties 去掉后缀部分)
 * cache.cacheType: 默认为redis,此配置值荐来自于application.properties
 */
public class Cache {

    //	private String remoteCacheApplication;
//
//	private String localCacheApplication;
//	private String localCacheSpec;
    private boolean inited = false;
    ICache cache = null;
    ICache local = null;
    IRedis redis = null;

    String clazz = "Cache";

    static Object obj = new Object();


    private static Map<String, Cache> cs = new ConcurrentHashMap<String, Cache>();


    public Cache(String cacheApplication) {
        init(cacheApplication);
    }

    public static Cache instance(String cacheApplication) {
        try {
            if (cs.containsKey(cacheApplication))
                return cs.get(cacheApplication);

            synchronized (obj) {
                if (cs.containsKey(cacheApplication))
                    return cs.get(cacheApplication);
                Cache cache = new Cache(cacheApplication);
                return cache;
            }
        }
        catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }


    public void init(String cacheApplication) {
        System.out.println("远程缓存初始化：instance(cacheApplication).init:" + cacheApplication);
        long t = System.currentTimeMillis();
        try {
            if (cs.containsKey(cacheApplication)) return;
            synchronized (obj) {
                if (cs.containsKey(cacheApplication))
                    return;
                if (null == cacheApplication || cacheApplication.trim().length() == 0)
                    throw new Exception("远程缓存初始化：remoteCacheApplication 为空");
                int eable = Config.get(cacheApplication, "eable", 1);
                if (eable < 1) {
                    System.out.println("远程缓存初始化，缓存已禁用:initRemote.cacheApplication:" + cacheApplication);
                    //return null;
                }
                //c=cache;
                //if(null!=c) return c;
                String cacheType = Config.get(cacheApplication, "cache.cacheType", "redis").toLowerCase().trim();
                System.out.println("远程缓存初始化：instance(cacheApplication).cacheType:" + cacheType);
                String[] cts = cacheType.split("\\|");
                if (cts.length > 1) {
                    cacheType = cts[0];
                    if (cacheType.equals("local"))
                        cacheType = cts[1];
                }
                String localCacheSpec = Config.get(cacheApplication, "cache.localCacheSpec", "").toLowerCase().trim();
                switch (cacheType) {
                    case "redissentine":
                        cache = new RedisSentineCache(cacheApplication);
                        redis = (IRedis) cache;
                        break;
                    case "redis":
                        cache = new RedisCache(cacheApplication);
                        redis = (IRedis) cache;
                        break;

                    case "rediscluster":
                        cache = new RedisClusterCache(cacheApplication);
                        redis = (IRedis) cache;
                        break;
                    case "local":
                        cache = new LocalCache(localCacheSpec);
                        break;
                    case "codis":
                        cache = new CodisCache(cacheApplication);
                        redis = (IRedis) cache;
                        break;
                    default:
                        cache = new RedisCache(cacheApplication);
                        redis = (IRedis) cache;
                        break;
                }
                if (cts.length > 1)
                    cache = new LocalTwoLevelCache(localCacheSpec, cache);
                cs.put(cacheApplication, this);
                inited = true;
            }
        } catch (Exception e) {
            //Log.write(LogAction.Error, this.getClass().getName(), "init", "cache", System.currentTimeMillis()-t ,e);
            e.printStackTrace();
        }
        //return c;
    }

    public boolean isInited() {
        return inited;
    }

    /**
     * 根据key获取,上果走json序列化,请不要用这个方法
     *
     * @param key，最终存储的key是cacheApplication+"_"+key
     * @return
     */
    public <T> T get(String key) {
        try {
            return cache.get(key);
        } catch (Exception e) {
            e.printStackTrace();
            remove(key);
        }
        return null;
    }


    public <T> T get(String key, Class<T> claz) {
        try {

            return cache.get(key, claz);
        } catch (Exception e) {
            remove(key);
            e.printStackTrace();
        }
        return null;
    }

    public <T> T hget(String key, String member, Class<T> claz) {
        try {

            return redis.hget(key, member, claz);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String hget(String key, String member) {
        try {

            return redis.hget(key, member);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    public <T> T get(String key, TypeReference tf) {
        ////Log.write(LogAction.Get, clazz, "get", "cache", 0 ,"key:"+key +" cache:"+cache.getClass().getName());
        try {

            return cache.get(key, tf);
        } catch (Exception e) {
            remove(key);
            e.printStackTrace();
        }
        return null;
    }

    public <T> List<T> hvals(String key, Class<T> claz) {

        return redis.havls(key, claz);
    }

    /**
     * 将key，value缓存，按照默认过期策略，过期分情况
     * <p>
     * 1.rediscache：使用cacheconfig.properties中的defaultExprie，如果未设置，使用全局默认值1200秒
     * </p>
     * <p>
     * 2.localcache：使用LRU策略，自动过期
     * </p>
     *
     * @param key
     * @param obj
     */
    public <T> String set(String key, T obj) {

        try {

            return cache.set(key, obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Fail";
    }


    public <T> long hset(String key, String member, T value) {
        try {

            return redis.hset(key, member, value);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    public Long hsetnx(final String key, final String field, final String value) {
        try {

            return redis.hsetnx(key, field, value);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1L;
    }

    public Long hincrBy(final String key, final String field, final long value) {
        try {

            return redis.hincrBy(key, field, value);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1L;
    }

    /**
     * 将key，value缓存，指定过期时间间隔，分情况
     * <p>
     * 1.rediscache：按照过期时间间隔，单位秒
     * </p>
     * <p>
     * 2.localcache：使用LRU策略，自动过期
     * </p>
     *
     * @param key
     * @param obj
     * @param seconds 如果 seconds=-1 则表示永不过期
     */
    public <T> String set(String key, T obj, int seconds) {
        try {

            return cache.set(key, obj, seconds);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Fail";
    }

    /**
     * 将key，value缓存，指定过期时间，分情况
     * <p>
     * 1.rediscache：按照过期时间，到指定expDate自动过期
     * </p>
     * <p>
     * 2.localcache：使用LRU策略，自动过期
     * </p>
     *
     * @param key
     * @param obj
     * @param expDate
     */
    public <T> String set(String key, T obj, Date expDate) {

        try {

            return cache.set(key, obj, expDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Fail";
    }

    public long remove(String... keys) {
        try {

            return cache.remove(keys);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    public List<Object> watch(String key, String value) {
        try {

            return redis.watch(key, value);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public <T> long setnx(String key, T value) {
        try {

            return redis.setnx(key, value);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    public long sadd(String key, String... members) {
        try {

            return redis.sadd(key, members);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    public Set<String> smembers(String key) {

        try {

            return redis.smembers(key);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    public Set<String> spop(String key, long count) {

        try {

            return redis.spop(key, count);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public long srem(String key, String... members) {
        try {

            return redis.srem(key, members);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    public long lock(String key, Object value, int seconds) {
        try {

            long rtn = 0;
            rtn = redis.setnx(key, value.toString());
            if (rtn > 0) redis.expire(key, seconds);
            return rtn;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    public long unlock(String... keys) {
        try {

            long rtn = 0;
            rtn = redis.del(keys);
            return rtn;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    public long del(String... keys) {
        try {

            return redis.del(keys);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * 设置过期时间
     *
     * @param key
     * @param seconds
     * @return
     */
    public long expire(String key, int seconds) {

        try {

            return redis.expire(key, seconds);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    /**
     * 计数器
     *
     * @param key
     * @param step
     * @return
     */
    public long incrBy(String key, long step) {
        try {

            return redis.incrBy(key, step);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    public <T> T getSet(String key, T value) {
        try {

            return redis.getSet(key, value);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取模糊匹配的Key集合
     *
     * @param pattern
     * @return
     */
    public Set<String> keys(String pattern) {
        try {

            return redis.keys(pattern);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 用户计数器重新开始
     *
     * @param key
     * @param step
     * @return
     */
    public long decrBy(String key, long step) {
        try {

            return redis.decrBy(key, step);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

    /**
     *
     */
    public Long rpush(String key, String... strings) {
        try {

            return redis.rpush(key, strings);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1L;
    }

    /**
     *
     */
    public String lpop(String key) {
        try {

            return redis.lpop(key);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     *
     */
    public List<String> lrange(final String key, final long start, final long end) {
        try {

            return redis.lrange(key, start, end);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     *
     */
    public List<String> lpopTrans(final String key, final long count) {
        try {
            return redis.lpopTrans(key, count);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     *
     */
    public List<Object> lpopPipelined(final String key, final long count) {
        try {

            return redis.lpopPipelined(key, count);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 返回缓存中列表的长度,
     * key对应类型不是list类型，返回空
     *
     * @param key
     * @return
     */
    public Long llen(String key) {
        Jedis jedis = null;
        long t = System.currentTimeMillis();
        try {
            return redis.llen(key);
        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, this.getClass().getName(), "keys", "cache", System.currentTimeMillis()-t ,e);
            return null;
        }
    }

}

