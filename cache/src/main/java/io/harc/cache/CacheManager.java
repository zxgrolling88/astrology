package io.harc.cache;

import com.fasterxml.jackson.core.type.TypeReference;

import java.util.*;

/**
 * @author chenliangtaiyi
 * 默认缓存的管理类,如果需要创建多实例用instance方法,传入配置文件的名字.(XXX.properties 去掉后缀部分)
 */
public class CacheManager {

    String clazz = "CacheManager";
    static CacheKeyInterface keymanager = CacheKeyManager.getManager();

    static int defaultRefreshSeconds = 600;
    static String refreshkey = ":^P^:";
    static int refreshLimiting = 1;
    static String refreshLimitingKey = "^cacheRefreshLimitingKey^";
    int type = 1;//1 默认  2 指定远程  3  默认二级  4 指定二级

    static int clearThredNum = 0; //防止清除运作时间过长,导致开过多线程. ":list:"

    public CacheManager() {

    }


    public static long clear(String key) {
        return clearCache(key, 0);
    }

    public static long clearCache(String key, int force) {
        return  clearCache(CacheConfig.CacheConfigName,key,force);
    }


    /**
     * 根据key获取单个对象     *
     *
     * @param key
     * @return
     */
    public static <T> T get(String key) {
        return get(key, defaultRefreshSeconds);
    }

    public static <T> T get(String key, int refreshSeconds) {
        System.out.println("cachemanager.get:" + key);
        Cache cache = Cache.instance(CacheConfig.CacheConfigName);
        if (key.indexOf(CacheKeyType.KEEPREFRESH.getKey()) > -1) {
            String kp = key + refreshkey;
            String r = cache.get(kp);
            if (null == r && cache.lock(refreshLimitingKey, refreshLimiting, 1) > 0 && cache.lock(kp, "0", refreshSeconds) > 0) {
                return null;
            }
        }
        return cache.get(key);
    }


    /**
     * 根据key获取List,Set,Map集合类对象,或者包含集合类的对象.     *
     *
     * @param key
     * @param claz 类名申明
     * @return
     */
    public static <T> T get(String key, Class<T> claz) {
        return get(key, claz, 600);
    }


    public static <T> T get(String key, Class<T> claz, int refreshSeconds) {
        Cache cache = Cache.instance(CacheConfig.CacheConfigName);
        if (key.indexOf(CacheKeyType.KEEPREFRESH.getKey()) > -1) {
            String kp = key + refreshkey;
            String r = cache.get(kp);
            if (null == r && cache.lock(kp, "0", refreshSeconds) > 0) {
                return null;
            }
        }
        return cache.get(key, claz);
    }


    public static <T> T get(String key, TypeReference tf) {
        Cache cache = Cache.instance(CacheConfig.CacheConfigName);
        return cache.get(key, tf);
    }

    public static <T> T hget(String key, String member, Class<T> claz) {
        //对比待清除列表.有的话直接删除前等清除列表中当前的Key.
        Cache cache = Cache.instance(CacheConfig.CacheConfigName);
        return cache.hget(key, member, claz);
    }

    public static <T> List<T> hvals(String key, Class<T> claz) {
        //对比待清除列表.有的话直接删除前等清除列表中当前的Key.
        Cache cache = Cache.instance(CacheConfig.CacheConfigName);
        return cache.hvals(key, claz);
    }

    /**
     * 将key，value缓存，按照默认过期策略，过期分情况
     * <p>
     * 1.rediscache：使用cacheconfig.properties中的defaultExprie，如果未设置，使用全局默认值1200秒
     * </p>
     * <p>
     * 2.localcache：使用LRU策略，自动过期
     * </p>
     *
     * @param key
     * @param obj
     */
    public static <T> String set(String key, T obj) {
        Cache cache = Cache.instance(CacheConfig.CacheConfigName);
        if (key.indexOf(CacheKeyType.KEEPREFRESH.getKey()) > -1) {
            cache.unlock(refreshLimitingKey);
        }
        String res = cache.set(key, obj);
        keymanager.addKey(cache, key);
        return res;
    }

    /**
     * 将key，value缓存，指定过期时间间隔，分情况
     * <p>
     * 1.rediscache：按照过期时间间隔，单位秒
     * </p>
     * <p>
     * 2.localcache：使用LRU策略，自动过期
     * </p>
     *
     * @param key
     * @param obj
     * @param seconds 如果 seconds=-1 则表示永不过期
     */
    public static <T> String set(String key, T obj, int seconds) {
        Cache cache = Cache.instance(CacheConfig.CacheConfigName);
        String res = cache.set(key, obj, seconds);
        keymanager.addKey(cache, key);
        return res;

    }

    /**
     * 将key，value缓存，指定过期时间，分情况
     * <p>
     * 1.rediscache：按照过期时间，到指定expDate自动过期
     * </p>
     * <p>
     * 2.localcache：使用LRU策略，自动过期
     * </p>
     *
     * @param key
     * @param obj
     * @param expDate
     */
    public static <T> String set(String key, T obj, Date expDate) {
        Cache cache = Cache.instance(CacheConfig.CacheConfigName);
        String res = cache.set(key, obj, expDate);
        keymanager.addKey(cache, key);
        return res;
    }

    public static <T> long hset(String key, String member, T value) {
        Cache cache = Cache.instance(CacheConfig.CacheConfigName);
        long res = cache.hset(key, member, value);
        keymanager.addKey(cache, key);
        return res;
    }

    /**
     * 设置过期时间
     *
     * @param key
     * @param seconds
     * @return
     */
    public static long expire(String key, int seconds) {
        Cache cache = Cache.instance(CacheConfig.CacheConfigName);
        return cache.expire(key, seconds);
    }

    public static long remove(String key) {
        Cache cache = Cache.instance(CacheConfig.CacheConfigName);
        return cache.remove(key);
    }

    public static long remove(String... keys) {
        Cache cache = Cache.instance(CacheConfig.CacheConfigName);
        return cache.remove(keys);
    }

    public static long sadd(String key, String... members) {
        Cache cache = Cache.instance(CacheConfig.CacheConfigName);
        return cache.sadd(key, members);
    }

    public static Set<String> smembers(String key) {
        Cache cache = Cache.instance(CacheConfig.CacheConfigName);
        return cache.smembers(key);
    }


    public static long lock(String key, String value, int seconds) {
        Cache cache = Cache.instance(CacheConfig.CacheConfigName);
        return cache.lock(key, value, seconds);
    }

    public static long unlock(String key) {
        Cache cache = Cache.instance(CacheConfig.CacheConfigName);
        return cache.unlock(key);
    }

    /**
     * 计数器
     *
     * @param key
     * @param step
     * @return
     */
    public static long incrBy(String key, long step) {
        Cache cache = Cache.instance(CacheConfig.CacheConfigName);
        return cache.incrBy(key, step);
    }

    /**
     * 用户计数器重新开始
     *
     * @param key
     * @param step
     * @return
     */
    public static long decrBy(String key, long step) {
        Cache cache = Cache.instance(CacheConfig.CacheConfigName);
        return cache.decrBy(key, step);
    }


    /**
     *
     */
    public static Set<String> keys(String key) {
        Cache cache = Cache.instance(CacheConfig.CacheConfigName);
        return cache.keys(key);
    }


    /**
     *
     */
    public static Long rpush(String key, String... strings) {
        Cache cache = Cache.instance(CacheConfig.CacheConfigName);
        return cache.rpush(key, strings);
    }

    /**
     *
     */
    public static String lpop(String key) {
        Cache cache = Cache.instance(CacheConfig.CacheConfigName);
        return cache.lpop(key);
    }

    /**
     *
     */
    public static List<String> lrange(final String key, final long start, final long end) {
        Cache cache = Cache.instance(CacheConfig.CacheConfigName);
        return cache.lrange(key, start, end);
    }

    /**
     *
     */
    public static List<String> lpopTrans(final String key, final long count) {
        Cache cache = Cache.instance(CacheConfig.CacheConfigName);
        return cache.lpopTrans(key, count);

    }

    /**
     *
     */
    public static List<Object> lpopPipelined(final String key, final long count) {
        Cache cache = Cache.instance(CacheConfig.CacheConfigName);
        return cache.lpopPipelined(key, count);
    }

    public static Long hsetnx(final String key, final String field, final String value) {
        Cache cache = Cache.instance(CacheConfig.CacheConfigName);
        return cache.hsetnx(key, field, value);
    }

    public static Long hincrBy(final String key, final String field, final long value) {
        Cache cache = Cache.instance(CacheConfig.CacheConfigName);
        return cache.hincrBy(key, field, value);
    }

    /**
     * 返回缓存中列表的长度,
     * key对应类型不是list类型，返回空
     *
     * @param key
     * @return
     */
    public static Long llen(final String key) {
        Cache cache = Cache.instance(CacheConfig.CacheConfigName);
        return cache.llen(key);
    }


    /**
     * ***************************************指定配置实例******************************************
     */








    public static long clear(String cacheConfig,String key) {
        return clearCache(cacheConfig,key, 0);
    }

    public static long clearCache(String cacheConfig,String key, int force) {
        long t = System.currentTimeMillis();
        long n = 0;
        Cache cache = Cache.instance(cacheConfig);
        System.out.println("开始清除缓存:" + key + " force:" + force);
        try {
            n = cache.remove(key);
            //Log.write(LogAction.Set, clazz, "clearCache", "cache", new Date().getTime() - t, "先精确匹配清除Key状态:" + key+ " n:"+n);

            int m = 0;
            if (n < 1) { //没有被精确清除,走模糊清除.这样的一般用于列表页的多页关联清除.
                System.out.println("n:" + n);
                key = key.replace("%s", "").replace("%d", "");
                m = keymanager.addClearKey(cache, key);
                if (force == 1 || m == 0)//没有被清除掉,再去糊糊查找,如果强制清除
                    clear(cacheConfig,key, force); //第一次清除会有锁等待,在这段时间内如果有清除Key进入队列,所有的进程就会放弃.
            } else
                System.out.println("精确清除了缓存:" + key);

        } catch (Exception e) {
            e.printStackTrace();
            //Log.write(LogAction.Error, clazz, "clearCache", "cache", new Date().getTime() - t, e);
            n = -1;
        }
        return n;
    }


    private static void clear(String cacheConfig,String key, int force) {
        long t = System.currentTimeMillis();
        Cache cache = Cache.instance(cacheConfig);
        long lock = cache.lock(CacheConfig.CacheClearLock, String.valueOf(t), CacheConfig.CacheClearTimeSpan);
        System.out.println("lock:" + lock + " CacheClearLock:" + CacheConfig.CacheClearLock + " CacheClearTimeSpan:" + CacheConfig.CacheClearTimeSpan);
        if (lock > 0) {//锁住缓存更新,至少保证1秒以上间隔更新.
            Set<String> setclears = cache.smembers(CacheConfig.CacheClearKeysKey);//取出要清理的列表
            Set<String> setcaches = cache.smembers(CacheConfig.CacheListKeysKey);//取出所有缓存key
            System.out.println(" 开始准备模糊清除:setclears.size():" + setclears.size() + " setcaches.size():" + setcaches.size() + " 是否强制:" + force);
            //Log.write(LogAction.Set, clazz, "clearCache", "cache", new Date().getTime() - t, " 开始准备模糊清除:setclears.size():" + setclears.size() +" setcaches.size():"+setcaches.size() +" 是否强制:"+force);
            if (clearThredNum < 3 && (force == 1 || setclears.size() > 0 && setcaches.size() > 0)) {
                Thread tr = new Thread(new Runnable() { //异步执行
                    public void run() {

                        long t = System.currentTimeMillis();
                        long n = 0;
                        clearThredNum++;
                        try {
                            if (force == 1) {
                                Set<String> keys = cache.keys(key);
                                if (null == keys || keys.size() == 0) {
                                    System.out.println("强制清除:没有模糊查找到Keys:" + key);
                                }
                                //Log.write(LogAction.Set, clazz, "clear", "cache", System.currentTimeMillis() - t, " 强制清除:没有模糊查找到Keys: "+key);
                                else {
                                    n = cache.remove(keys.toArray(new String[]{}));
                                    System.out.println(" 强制清除了key: " + key + " " + n + " 条缓存");
                                    //Log.write(LogAction.Set, clazz, "clear", "cache", System.currentTimeMillis() - t, " 强制清除了key: "+key+" " + n + " 条缓存");
                                }
                            } else {
                                List<String> delCaches = new ArrayList<String>();
                                for (String scache : setcaches) {
                                    for (String sclear : setclears) {
                                        if (scache.indexOf(sclear) > -1) {//如果缓存的Key如 XXXXX:%d 只有一个参数的情况.
                                            delCaches.add(scache);
                                            System.out.println(" 配成功:待清Key: " + sclear + " 被清除Key: " + scache);

                                            //Log.write(LogAction.Set, clazz, "clear", "cache", System.currentTimeMillis() - t, " 配成功:待清Key: " + sclear + " 被清除Key: " + scache  );
                                        } else {//如果要删除的Key每段字符都包含在待清除的key中,说明就模糊匹配上了.a:b:id:d%:name:s% 这样的的Key经过前一个方法中的替换后成为:  a:b:id:name
                                            String seg[] = sclear.split(":");
                                            int i = 0;
                                            for (String s : seg) {
                                                if (scache.indexOf(s + ":") > -1 || scache.indexOf(":" + s) > -1)
                                                    i++;
                                            }
                                            if (i == seg.length) //缓存key的每一段都匹配上了,说明是同一个缓存对象,仅是参数不一样.
                                                delCaches.add(scache);
                                        }
                                    }
                                }
                                if (delCaches.size() > 0) {
                                    String[] delCacheKeys = delCaches.toArray(new String[delCaches.size()]);
                                    String[] clearKeys = setclears.toArray(new String[setclears.size()]);
                                    n = cache.remove(delCacheKeys); //批量清除缓存
                                    cache.srem(CacheConfig.CacheListKeysKey, delCacheKeys);//批量清除缓存Key的列表.
                                    cache.srem(CacheConfig.CacheClearKeysKey, clearKeys);//删除清除的Key.
                                } else {
                                    System.out.println("没有匹配到清除的,等清除Key列表大小:" + delCaches.size() + " 所有缓存Key列表大小:" + setcaches.size());
                                }
                                //Log.write(LogAction.Set, clazz, "clear", "cache", System.currentTimeMillis() - t, "没有匹配到清除的,等清除Key列表大小:"+delCaches.size() +" 所有缓存Key列表大小:"+setcaches.size() );
                            }
                            System.out.println(" 成功清除: " + n + " 条缓存");

                            //Log.write(LogAction.Set, clazz, "clear", "cache", System.currentTimeMillis() - t, " 成功清除: " + n + " 条缓存");
                        } catch (Exception e) {
                            e.printStackTrace();
                            //Log.write(LogAction.Error, clazz, "clear", "cache", new Date().getTime() - t, e);
                        } finally {
                            --clearThredNum;
                            //cache.unlock(config.CacheClearLock());
                        }
                    }
                });
                tr.start();
            }
        }
    }


    /**
     * 根据key获取单个对象     *
     *
     * @param key
     * @return
     */
    public static <T> T get(String cacheConfig,String key) {
        return get(key, defaultRefreshSeconds);
    }

    public static <T> T get(String cacheConfig,String key, int refreshSeconds) {
        System.out.println("cachemanager.get:" + key);
        Cache cache = Cache.instance(cacheConfig);
        if (key.indexOf(CacheKeyType.KEEPREFRESH.getKey()) > -1) {
            String kp = key + refreshkey;
            String r = cache.get(kp);
            if (null == r && cache.lock(refreshLimitingKey, refreshLimiting, 1) > 0 && cache.lock(kp, "0", refreshSeconds) > 0) {
                return null;
            }
        }
        return cache.get(key);
    }


    /**
     * 根据key获取List,Set,Map集合类对象,或者包含集合类的对象.     *
     *
     * @param key
     * @param claz 类名申明
     * @return
     */
    public static <T> T get(String cacheConfig,String key, Class<T> claz) {
        return get(key, claz, 600);
    }


    public static <T> T get(String cacheConfig,String key, Class<T> claz, int refreshSeconds) {
        Cache cache = Cache.instance(cacheConfig);
        if (key.indexOf(CacheKeyType.KEEPREFRESH.getKey()) > -1) {
            String kp = key + refreshkey;
            String r = cache.get(kp);
            if (null == r && cache.lock(kp, "0", refreshSeconds) > 0) {
                return null;
            }
        }
        return cache.get(key, claz);
    }


    public static <T> T get(String cacheConfig,String key, TypeReference tf) {
        Cache cache = Cache.instance(cacheConfig);
        return cache.get(key, tf);
    }

    public static <T> T hget(String cacheConfig,String key, String member, Class<T> claz) {
        //对比待清除列表.有的话直接删除前等清除列表中当前的Key.
        Cache cache = Cache.instance(cacheConfig);
        return cache.hget(key, member, claz);
    }

    public static <T> List<T> hvals(String cacheConfig,String key, Class<T> claz) {
        //对比待清除列表.有的话直接删除前等清除列表中当前的Key.
        Cache cache = Cache.instance(cacheConfig);
        return cache.hvals(key, claz);
    }

    /**
     * 将key，value缓存，按照默认过期策略，过期分情况
     * <p>
     * 1.rediscache：使用cacheconfig.properties中的defaultExprie，如果未设置，使用全局默认值1200秒
     * </p>
     * <p>
     * 2.localcache：使用LRU策略，自动过期
     * </p>
     *
     * @param key
     * @param obj
     */
    public static <T> String set(String cacheConfig,String key, T obj) {
        Cache cache = Cache.instance(cacheConfig);
        if (key.indexOf(CacheKeyType.KEEPREFRESH.getKey()) > -1) {
            cache.unlock(refreshLimitingKey);
        }
        String res = cache.set(key, obj);
        keymanager.addKey(cache, key);
        return res;
    }

    /**
     * 将key，value缓存，指定过期时间间隔，分情况
     * <p>
     * 1.rediscache：按照过期时间间隔，单位秒
     * </p>
     * <p>
     * 2.localcache：使用LRU策略，自动过期
     * </p>
     *
     * @param key
     * @param obj
     * @param seconds 如果 seconds=-1 则表示永不过期
     */
    public static <T> String set(String cacheConfig,String key, T obj, int seconds) {
        Cache cache = Cache.instance(cacheConfig);
        String res = cache.set(key, obj, seconds);
        keymanager.addKey(cache, key);
        return res;

    }

    /**
     * 将key，value缓存，指定过期时间，分情况
     * <p>
     * 1.rediscache：按照过期时间，到指定expDate自动过期
     * </p>
     * <p>
     * 2.localcache：使用LRU策略，自动过期
     * </p>
     *
     * @param key
     * @param obj
     * @param expDate
     */
    public static <T> String set(String cacheConfig,String key, T obj, Date expDate) {
        Cache cache = Cache.instance(cacheConfig);
        String res = cache.set(key, obj, expDate);
        keymanager.addKey(cache, key);
        return res;
    }

    public static <T> long hset(String cacheConfig,String key, String member, T value) {
        Cache cache = Cache.instance(cacheConfig);
        long res = cache.hset(key, member, value);
        keymanager.addKey(cache, key);
        return res;
    }

    /**
     * 设置过期时间
     *
     * @param key
     * @param seconds
     * @return
     */
    public static long expire(String cacheConfig,String key, int seconds) {
        Cache cache = Cache.instance(cacheConfig);
        return cache.expire(key, seconds);
    }

    public static long remove(String cacheConfig,String key) {
        Cache cache = Cache.instance(cacheConfig);
        return cache.remove(key);
    }

    public static long remove(String cacheConfig,String... keys) {
        Cache cache = Cache.instance(cacheConfig);
        return cache.remove(keys);
    }

    public static long sadd(String cacheConfig,String key, String... members) {
        Cache cache = Cache.instance(cacheConfig);
        return cache.sadd(key, members);
    }

    public static Set<String> smembers(String cacheConfig,String key) {
        Cache cache = Cache.instance(cacheConfig);
        return cache.smembers(key);
    }


    public static long lock(String cacheConfig,String key, String value, int seconds) {
        Cache cache = Cache.instance(cacheConfig);
        return cache.lock(key, value, seconds);
    }

    public static long unlock(String cacheConfig,String key) {
        Cache cache = Cache.instance(cacheConfig);
        return cache.unlock(key);
    }

    /**
     * 计数器
     *
     * @param key
     * @param step
     * @return
     */
    public static long incrBy(String cacheConfig,String key, long step) {
        Cache cache = Cache.instance(cacheConfig);
        return cache.incrBy(key, step);
    }

    /**
     * 用户计数器重新开始
     *
     * @param key
     * @param step
     * @return
     */
    public static long decrBy(String cacheConfig,String key, long step) {
        Cache cache = Cache.instance(cacheConfig);
        return cache.decrBy(key, step);
    }


    /**
     *
     */
    public static Set<String> keys(String cacheConfig,String key) {
        Cache cache = Cache.instance(cacheConfig);
        return cache.keys(key);
    }


    /**
     *
     */
    public static Long rpush(String cacheConfig,String key, String... strings) {
        Cache cache = Cache.instance(cacheConfig);
        return cache.rpush(key, strings);
    }

    /**
     *
     */
    public static String lpop(String cacheConfig,String key) {
        Cache cache = Cache.instance(cacheConfig);
        return cache.lpop(key);
    }

    /**
     *
     */
    public static List<String> lrange(String cacheConfig,final String key, final long start, final long end) {
        Cache cache = Cache.instance(cacheConfig);
        return cache.lrange(key, start, end);
    }

    /**
     *
     */
    public static List<String> lpopTrans(String cacheConfig,final String key, final long count) {
        Cache cache = Cache.instance(cacheConfig);
        return cache.lpopTrans(key, count);

    }

    /**
     *
     */
    public static List<Object> lpopPipelined(String cacheConfig,final String key, final long count) {
        Cache cache = Cache.instance(cacheConfig);
        return cache.lpopPipelined(key, count);
    }

    public static Long hsetnx(String cacheConfig,final String key, final String field, final String value) {
        Cache cache = Cache.instance(cacheConfig);
        return cache.hsetnx(key, field, value);
    }

    public static Long hincrBy(String cacheConfig,final String key, final String field, final long value) {
        Cache cache = Cache.instance(cacheConfig);
        return cache.hincrBy(key, field, value);
    }

    /**
     * 返回缓存中列表的长度,
     * key对应类型不是list类型，返回空
     *
     * @param key
     * @return
     */
    public static Long llen(String cacheConfig,final String key) {
        Cache cache = Cache.instance(cacheConfig);
        return cache.llen(key);
    }




}
