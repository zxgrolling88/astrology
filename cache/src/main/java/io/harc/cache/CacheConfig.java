package io.harc.cache;

import io.harc.common.Config;
import io.harc.common.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * @author: chenliangtaiyi
 * 从当前环境读取 cache 的配置文件,如果没有配置或者为空白.则其它配置项从当前环境配置读取.
 */
@Configuration
@Component
public class CacheConfig extends Config {



    private final  String clazz = CacheConfig.class.getName();

    /**
     * 缓存文件配置.
     */
    @Value("${cache.configName:cache}")
    String configName;




    @Value("${cache.preifx:}")
    String prefix;

    @Value("${cache.clearTimeSpan:3}")
    int clearTimeSpan;


    public String getConfigName() {
        return configName;
    }

    public String getPrefix() {
        return prefix;
    }

    public int getClearTimeSpan() {
        return clearTimeSpan;
    }

    @PostConstruct
    public void init() {
        //super.init();
        if(null==configName || configName.equals("")){
            configName=DEFAULT_CONFIG_NAME;
        }
        configName=appendActive(configName.trim());
        CacheConfigName=configName;
        if(prefix.equals(""))
            prefix=APPNAME;
        System.out.println("DEFAULT_CONFIG_NAME:"+DEFAULT_CONFIG_NAME+" cache 配置:cache.configName: " + configName );
        CachePrefix = get(configName, "cache.prefix", prefix)+":"+activeProfile;
        CacheClearTimeSpan = get(configName, "cache.clearTimeSpan", clearTimeSpan);
        CacheListPrefix = CachePrefix + CacheKeyType.LIST.getKey();
        CacheItemPrefix = CachePrefix + CacheKeyType.ITEM.getKey();
        CacheListKeysKey = CachePrefix + ":CacheListKeysKey";
        CacheClearKeysKey = CachePrefix + ":CacheClearKeysKey";
        CacheClearLock = CachePrefix + ":CacheClearLock";
    }

    public static String CacheConfigName;
    public static String CachePrefix;
    public static int    CacheClearTimeSpan;
    public static String CacheListPrefix;
    public static String CacheItemPrefix;
    public static String CacheListKeysKey;
    public static String CacheClearKeysKey;
    public static String CacheClearLock;

    public static   int CacheOneHoure=60*60;
    public static  int CacheOneDay=60*60*24;
    public static int CacheOneThreeDay=60*60*24*3;
    public static  int CacheOneWeek=60*60*24*7;
    public static  int CacheOneMonth=60*60*24*30;


}
