package io.harc.cache;

/**
 * Created by wanghanbing on 2017/2/9.
 * 缓存key的管理接口定义
 */
public interface CacheKeyInterface {

    public void addKey(Cache cache, String key);

    /**
     *
     * @param key
     * @return 0 表示添加成功 1 表示失败
     */
    public int addClearKey(Cache cache, String key);
}
