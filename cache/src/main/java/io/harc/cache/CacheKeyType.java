package io.harc.cache;

/**
 * Created by wanghanbing on 2017/2/9.
 * 缓存Key的类型List, Item 分别代表 模糊和精确.
 */
public enum CacheKeyType {
    LIST(":list:", "列表模糊匹配类型的cache key"),
    ITEM(":item:", "item完全匹配类型的cache key"),
    KEEPREFRESH(":keeprefresh:","保持刷新的key");

    private String key;
    private String desc;

    CacheKeyType(String key, String desc) {
        this.key = key;
        this.desc = desc;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
