package io.harc.cache.interfaces;

import java.util.Date;
import java.util.Set;

import com.fasterxml.jackson.core.type.TypeReference;


/**
 * @author: chenliangtaiyi
 * 缓存接口定义
 */

 public interface ICache {

	 <T> T get(String key);
	
	 <T> T get(String key, Class<T> claz); 
	
	 <T> T get(String key, TypeReference tf);

	 <T> String set(String key, T obj);// after seconds expire

	 <T> String set(String key, T obj, int seconds);// after seconds expire

	 <T> String set(String key, T obj, Date expDate);// expire date
	
	 long remove(String... keys);



}
