package io.harc.cache.interfaces;

import io.harc.cache.Cache;

/**
 * Created by wanghanbing on 2017/2/9.
 * 缓存的Key管理
 */
public interface ICacheKey {

    public void addKey(Cache cache, String key);

    /**
     * 
     * @param cache
     * @param key
     * @return 0 表示添加成功 1 表示失败
     */
    public int addClearKey(Cache cache, String key);
}
