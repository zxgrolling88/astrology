package io.harc.cache.interfaces;

import com.fasterxml.jackson.core.type.TypeReference;

/**
 * @author: chenliangtaiyi
 * 缓存的序列化定义
 */

public interface ISerialize {

	 
	  byte[] serialize(Object value) throws  Exception;
 
	  <T> T deserialize(byte[] bytes) throws  Exception ;
	  
	  <T> T deserialize(byte[] bytes, Class<T> claz) throws  Exception ;


	  <T> T deserialize(byte[] bytes, TypeReference tf) throws  Exception ;

	   
}
