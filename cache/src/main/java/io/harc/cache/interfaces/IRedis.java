package io.harc.cache.interfaces;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author: chenliangtaiyi
 * redis相关操作
 */
public interface IRedis {

    /**
     * 乐观锁获取获取时间戳(long)
     *
     * @param key
     * @return 当前时间戳, 如果为0说明 没抢到锁,如果为-1表示异常.
     */
    List<Object> watch(String key, String value);

    long sadd(String key, String... members);

    Set<String> smembers(String key);

    Set<String> spop(String key, long count);

    long del(String... keys);

    long setnx(String key, String value);

    long expire(String key, int seconds);

    long srem(String key, String... members);

    long hset(String key, String member, String value);

    <T> long hset(String key, String member, T value);

    Long hsetnx(final String key, final String field, final String value);

    Map<String, String> hgetAll(String key);

    <T> Map<String, T> hgetAll(String key, Class<T> claz);

    public List<String> hvals(String key);

    public <T> List<T> havls(String key, Class<T> claz);

    String hget(String key, String member);

    <T> T hget(String key, String member, Class<T> claz);

    boolean hexists(String key, String member);

    long hdel(String key, final String... members);

    Long hincrBy(final String key, final String field, final long value);

    long lock(String key, String value, int seconds);

    long unlock(String... keys);


    <T> T getSet(String key, T obj);

    <T> long setnx(String key, T obj);

    long incrBy(String key, long step);

    long decrBy(String key, long step);

    Set<String> keys(String pattern);

    Long rpush(final String key, final String... strings);

    Long lpush(final String key, final String... strings);

    Long llen(final String key);

    List<String> lrange(final String key, final long start, final long end);

    String ltrim(final String key, final long start, final long end);

    String lindex(final String key, final long index);

    String lset(final String key, final long index, final String value);

    Long lrem(final String key, final long count, final String value);

    String lpop(final String key);

    String rpop(final String key);

    String rpoplpush(final String srckey, final String dstkey);

    // 使用事务的方式从list中pop多个数据
    List<String> lpopTrans(final String key, final long Count);

//    使用Pipelined的方式连续获取list列表的数据
    List<Object> lpopPipelined(final String key, final long count);
}
