/*
SQLyog Enterprise Trial - MySQL GUI v7.11 
MySQL - 5.7.25-log : Database - jana
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`jana` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `jana`;

/*Table structure for table `orders` */

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) DEFAULT NULL,
  `orderId` varchar(255) DEFAULT NULL,
  `payType` int(32) NOT NULL,
  `amount` double(20,2) NOT NULL,
  `totalAmount` double(20,2) NOT NULL,
  `userUid` varchar(255) DEFAULT NULL,
  `userName` varchar(255) DEFAULT NULL,
  `nickName` varchar(255) DEFAULT NULL,
  `payState` int(32) NOT NULL,
  `payTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `checkCode` varchar(255) DEFAULT NULL,
  `subject` varchar(255) DEFAULT NULL,
  `productCode` varchar(255) DEFAULT NULL,
  `updateTime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `merchantOrderNo` varchar(255) DEFAULT NULL,
  `outTradeNo` varchar(255) DEFAULT NULL,
  `sellerId` varchar(255) DEFAULT NULL,
  `tradeNo` varchar(255) DEFAULT NULL,
  `code` varchar(255) DEFAULT NULL,
  `msg` varchar(255) DEFAULT NULL,
  `subCode` varchar(255) DEFAULT NULL,
  `subMsg` varchar(255) DEFAULT NULL,
  `notifyTime` datetime DEFAULT CURRENT_TIMESTAMP,
  `gmtCreate` datetime DEFAULT CURRENT_TIMESTAMP,
  `gmtPayment` datetime DEFAULT CURRENT_TIMESTAMP,
  `gmtRefund` datetime DEFAULT CURRENT_TIMESTAMP,
  `gmtClose` datetime DEFAULT CURRENT_TIMESTAMP,
  `sellerEmail` varchar(255) DEFAULT NULL,
  `outBizNo` varchar(255) DEFAULT NULL,
  `sign` varchar(1024) DEFAULT NULL,
  `buyerLogonId` varchar(255) DEFAULT NULL,
  `receiptAmount` varchar(255) DEFAULT NULL,
  `invoiceAmount` varchar(255) DEFAULT NULL,
  `buyerPayAmount` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `orders` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(32) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) DEFAULT NULL,
  `userName` varchar(255) DEFAULT NULL,
  `nickName` varchar(255) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `roles` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `features` varchar(255) DEFAULT NULL,
  `style` varchar(255) DEFAULT NULL,
  `lat` float NOT NULL,
  `lon` float NOT NULL,
  `country` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `district` varchar(255) DEFAULT NULL,
  `quarter` varchar(255) DEFAULT NULL,
  `building` varchar(255) DEFAULT NULL,
  `floor` varchar(255) DEFAULT NULL,
  `houseNo` varchar(255) DEFAULT NULL,
  `workProvince` varchar(255) DEFAULT NULL,
  `workCity` varchar(255) DEFAULT NULL,
  `workDistrict` varchar(255) DEFAULT NULL,
  `workPark` varchar(255) DEFAULT NULL,
  `workBuilding` varchar(255) DEFAULT NULL,
  `workFloor` varchar(255) DEFAULT NULL,
  `workHouseNo` varchar(255) DEFAULT NULL,
  `workLocCode` varchar(255) DEFAULT NULL,
  `diviceId` varchar(255) DEFAULT NULL,
  `mobileType` varchar(255) DEFAULT NULL,
  `hungryDate` datetime DEFAULT CURRENT_TIMESTAMP,
  `updateTime` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `token` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

/*Data for the table `users` */

insert  into `users`(`id`,`uid`,`user_name`,`nick_name`,`gender`,`roles`,`phone`,`avatar`,`features`,`style`,`lat`,`lon`,`country`,`province`,`city`,`district`,`quarter`,`building`,`floor`,`house_no`,`work_province`,`work_city`,`work_district`,`work_park`,`work_building`,`work_floor`,`work_house_no`,`work_loc_code`,`divice_id`,`mobile_type`,`hungry_date`,`update_time`,`token`) values (1,'f034f07b-4aed-44d8-be00-9833399d40c5','333333','333333','1','Foodie','13988810101','111111','111111','111111',1,1,'111111','111111','111111','111111','111111','111111','111111','111111','111111','111111','111111','111111','111111','111111','111111','b70ee756e3a7dde8cc12601aa3ab61a26bf64bc6','111111','111111','2021-04-15 13:02:25','2021-04-15 13:02:25','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJmMDM0ZjA3Yi00YWVkLTQ0ZDgtYmUwMC05ODMzMzk5ZDQwYzUifQ.MGXm8fxEVeRm4eyXRTSyGWOVQpH8K8UL2cZynDohejs'),(2,'f46dfc4c-8ade-4f13-b4d2-715d5423f140','444444','444444','1','Foodie','13988810101','111111','111111','111111',1,1,'111111','111111','111111','111111','111111','111111','111111','111111','111111','111111','111111','111111','111111','111111','111111','b70ee756e3a7dde8cc12601aa3ab61a26bf64bc6','111111','111111','2021-04-15 13:02:51','2021-04-15 13:02:51','eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJmNDZkZmM0Yy04YWRlLTRmMTMtYjRkMi03MTVkNTQyM2YxNDAifQ._EOzKFBMPjVTsrN2qcuViudyTU_jx_OdV9wDjCgGmXE');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
