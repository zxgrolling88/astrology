package io.harc.common;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;


/**
 * 日期Util类
 */
public class DateUtil {
    static  SimpleDateFormat shortFormat=new SimpleDateFormat("yyyy-MM-dd");
    static  SimpleDateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    static  SimpleDateFormat longFormat=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

    public static Date getLocalDate(String timeZone, Date date) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        TimeZone tz = TimeZone.getTimeZone("timeZone");
        calendar.setTimeZone(tz);
        return calendar.getTime();
    }

    public static Date getUTCDate( Date date) {
        Calendar cal = Calendar.getInstance();
        //获得时区和 GMT-0 的时间差,偏移量
        int offset = cal.get(Calendar.ZONE_OFFSET);
        //获得夏令时  时差
        int dstoff = cal.get(Calendar.DST_OFFSET);
        cal.add(Calendar.MILLISECOND, - (offset + dstoff));
        return cal.getTime();
    }

    public static Date getNowUTCDate( ) {
         return getUTCDate(new Date());
    }

    public static Date  getDate(Date date){
        try {
            return shortFormat.parse(shortFormat.format(date));
        }
        catch (Exception  e){
            return   null;
        }
    }



    public static Timestamp getDiffDayTimeStamp(Date date, int days, String format) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, days);
        date = calendar.getTime();
        DateFormat sdf = new SimpleDateFormat(format);
        Timestamp stamp = Timestamp.valueOf(sdf.format(date));
        return stamp;
    }

    public static Timestamp getDiffHourTimeStamp(Date date, int hours, String format) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR, hours);
        date = calendar.getTime();
        DateFormat sdf = new SimpleDateFormat(format);
        Timestamp stamp = Timestamp.valueOf(sdf.format(date));
        return stamp;
    }

    public static Date getDiffDayDate(Date date, int days, String format) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, days);
        date = calendar.getTime();
        return date;
    }


    public static Date getDiffHourDate(Date date, int hours, String format) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(Calendar.HOUR, hours);
        date = calendar.getTime();
        return date;
    }

    public static String toShortString(Date date) {
        return shortFormat.format(date);
    }

    public static String toMiddleString(Date date) {
        return dateFormat.format(date);
    }


    public static String toLongString(Date date) {
        return longFormat.format(date);
    }

    public static Date parse(String date)   {
        try {
            return dateFormat.parse(date);
        }
        catch (Exception e){
            return null;
        }
    }

    public static Date addMonth(Date date, int n) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, n);
        return cal.getTime();
    }
    public static Date addDay(Date date, int n) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, n);
        return cal.getTime();
    }

    public static Date addHour(Date date, int n) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.HOUR, n);
        return cal.getTime();
    }

}  