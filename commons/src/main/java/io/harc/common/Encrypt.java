package io.harc.common;

import java.awt.im.InputContext;
import java.security.MessageDigest;

public class Encrypt {


    public static String SHA1(String source)  {
        try {
            byte[] bytes = MessageDigest.getInstance("SHA-1").digest(source.getBytes("UTF-8"));
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();
        }
        catch (Exception e){
            return e.getMessage();
        }
    }
}
