package io.harc.common;

import java.util.Date;


public class Constants {

    public static final String LOG_FORMAT="time="+DateUtil.toLongString(new Date())+", class=%s, method=%s, key=%s, value=%s, message={\"%s\"}";

    public static final String CHARSET = "UTF-8";
    public static final String KEY_ALGORITHM_SHA512 = "SHA-512";
    public static final String KEY_ALGORITHM_MD5 = "MD5";
    public static final String KEY_ALGORITHM_SHA1 = "SHA-1";
    public static final String KEY_ALGORITHM_3DES = "DESede";
    public static final String KEY_3DES_TRANS = "DESede/ECB/PKCS5Padding";
    public static final String KEY_3DES_KEY = "XYCM3DES0987654321@2016";

    public static final char[] DIGITS_LOWER = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final long TIME_OF_MINUTE = 60000L;
    public static final long TIME_OF_HOUR = 3600000L;
    public static final long TIME_OF_DAY = 86400000L;


}


