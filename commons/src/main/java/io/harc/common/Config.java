package io.harc.common;

import org.springframework.beans.factory.annotation.Value;

import org.springframework.context.annotation.Configuration;
import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author chenliang
 */

@Configuration
public class Config  {

    public static String APPNAME="harc";
    public static String DEFAULT_CONFIG_NAME="application";

    public static String DEFAULTSCRIPTSNAME = "scripts";

    public static String DEFAULTJDBCNAME = "jdbc.xml";

    public static String JNDIFILENAME = "context.xml";

    @Value("${application.name:harc}")
    public  String applicationName;

    @Value("${application.configType:file}")
    public  String configType;


    @Value("${application.activeFormate:{0}-{1}}")
    public  String activeFormate;


    @Value("${spring.profiles.active:dev}")
    public  String activeProfile;


 	protected static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

   private static Map<String, Properties> configs = new ConcurrentHashMap<String, Properties>();

   static String configCenterHost = Config.get("configCenterHost", "config.yanxuan.com");

   static Config cnf=null;


   private static String configurationType ="file"; // 0 走配置文件  1 走配置中心
    private  static String activePathFormate="{0}-{1}";


    public static String Active;

    @PostConstruct
    public void init() {
        Active=activeProfile;
        APPNAME=applicationName;
        activePathFormate=activeFormate;
        configurationType=configType;
    }



     public static void  init(String configName){
    	if(configs.containsKey(configName)) return;
    	//Map<String, Properties> properties = new HashMap<String, Properties>();
    	try {

            Enumeration<URL> ps = Thread.currentThread().getContextClassLoader().getResources(configName.trim()+".properties");
       	    if(null==ps || !ps.hasMoreElements())
       	     ps=Thread.currentThread().getContextClassLoader().getResources(appendActiveProfile(configName.trim())+".properties");


               InputStream in = null;
               InputStreamReader sr=null;
               try {
                    if(null!=ps  && ps.hasMoreElements()) {
                        in = ps.nextElement().openStream();
                        Properties p = new Properties();
                        sr = new InputStreamReader(in, "UTF-8");
                        p.load(sr);
                        configs.put(configName, p);
                    }
               } finally {
                   if (in != null) {
                       try {
                           in.close();
                       } catch (IOException e) {
                           e.printStackTrace();
                       }
                   }
               }
       } catch (Exception ex) {
    	    System.out.println(configName+".properties:"+ ex.getMessage());
            ex.printStackTrace();
       }
    }


     public   String appendActive(String configName){
         if(null==activeProfile || activeProfile.length()==0)
             return configName;
        return MessageFormat.format(activeFormate,configName,activeProfile);
    }


    public  static String appendActiveProfile(String configName){
        if(null==Active || Active.length()==0)
            return configName;
        return MessageFormat.format(activePathFormate,configName,Active);
    }


    public  static  String get(String key) {
        try {
            return  configs.get(DEFAULT_CONFIG_NAME).getProperty(key, "");
        } catch (Exception e) {
            return "";
        }
    }


    public  static  String get(String key, String defaultValue) {
        try {
            return  configs.get(DEFAULT_CONFIG_NAME).getProperty(key, defaultValue);
        } catch (Exception e) {
            return defaultValue;
        }
    }


    public   static long get(String key, long defaultValue) {
        try {
            return Long.parseLong(configs.get(DEFAULT_CONFIG_NAME).getProperty(key, defaultValue+""));
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public   static boolean get(String key, boolean defaultValue) {
        try {
            return Boolean.parseBoolean(configs.get(DEFAULT_CONFIG_NAME).getProperty(key, defaultValue+""));
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static  int get(String key, int defaultValue) {
        try {
            return Integer.parseInt(configs.get(DEFAULT_CONFIG_NAME).getProperty(key, defaultValue+""));
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static  double get(String key, double defaultValue) {
        try {
            return Double.parseDouble(configs.get(DEFAULT_CONFIG_NAME).getProperty(key, defaultValue+""));
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static  float get(String key, float defaultValue) {
        try {
            return Float.parseFloat(configs.get(DEFAULT_CONFIG_NAME).getProperty(key, defaultValue+""));
        } catch (Exception e) {
            return defaultValue;
        }
    }


    public static Date get(String key,Date defaultValue) {
        try {
            return sdf.parse(configs.get(DEFAULT_CONFIG_NAME).getProperty(key, sdf.format(defaultValue)));
        } catch (Exception e) {
             return defaultValue;
        }
    }




    public static    String get(String groupName,String key, String defaultValue) {
        init(groupName);
        if(null == configs.get(groupName)) {
            return defaultValue;
        }
        if(null == configs.get(groupName)) {
            return defaultValue;
        }
        return  configs.get(groupName).getProperty(key, defaultValue).trim();
    }

    public static    int get(String groupName,String key, int defaultValue) {
        init(groupName);
        if(null == configs.get(groupName)) {
            return defaultValue;
        }
        return Integer.parseInt(configs.get(groupName).getProperty(key, defaultValue+"").trim());
    }

    public static    boolean get(String groupName,String key, boolean defaultValue) {
        init(groupName);
        if(null == configs.get(groupName)) {
            return defaultValue;
        }
        return Boolean.parseBoolean(configs.get(groupName).getProperty(key, defaultValue+"").trim());
    }

//    protected   String getValue(String key, String defaultValue) {
//        return getValue(DEFAULT_CONFIG_NAME,key,defaultValue);
//         //   return  configs.get(DEFAULT_CONFIG_NAME).getProperty(key, defaultValue + "").trim();
//    }


//    protected   String getValue(String groupName,String key, String defaultValue) {
//        init(groupName);
//            return  configs.get(groupName).getProperty(key, defaultValue).trim();
//    }

    public static String getProcessName() {
        try {
            java.lang.management.RuntimeMXBean runtimeMXBean = java.lang.management.ManagementFactory
                    .getRuntimeMXBean();
            return runtimeMXBean.getName();
        } catch (Exception e) {
            return "";
        }
    }


}
