package io.harc.common;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;


public class StringUtils {


	public static boolean isBlank(String s){
		return null==s || s.length()==0;
	}


	public static boolean isEmpty(String s){
		return null==s || s.length()==0;
	}

	public static int parseInteger(String str) {
    	try{
    		return Integer.parseInt(str);
    	}
    	catch(Exception e){
    		return 0;
    	}
    }
    
    public static double parseDouble(String str) {
    	try{
    		return Double.parseDouble(str);
    	}
    	catch(Exception e){
    		return 0.0;
    	}
    }
    
    public static float parseFloat(String str) {
    	try{
    		return Float.parseFloat(str);
    	}
    	catch(Exception e){
    		return 0;
    	}
    }

    public static long parseLong(String str) {
        try{
            return Long.parseLong(str);
        }
        catch(Exception e){
            return 0;
        }
    }

	
}
