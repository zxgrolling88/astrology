package io.harc.common;

import java.io.Serializable;

public class    Result<T>  implements Serializable {
    private int code=-1;
    private String message;
    private T data;


    public Result(){

    }

    public Result(T data){
        this.code=ErrorCode.Success.getCode();
        this.message=ErrorCode.Success.getMessage();
        this.data=data;
    }

    public Result(Exception e){
        this.code=ErrorCode.Failure.getCode();
        this.message=e.getMessage();
    }


    public Result(int code,String message){
        this.code=code;
        this.message=message;
    }

    public Result(int code,String message,T data){
        this.code=code;
        this.message=message;
        this.data=data;
    }

    public boolean isSuccess() {
        return code==ErrorCode.Success.getCode();
    }

    public int getCode() {
        return code;
    }

    public Result setCode(int code) {
        this.code = code;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public Result setMessage(String message) {
        this.message = message;
        return this;
    }

    public T getData() {
        return data;
    }

    public Result<T> setData(T data) {
        this.data = data;
        return this;
    }

    @Override
    public String toString() {
        return "Result{" +
                ", code=" + code +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}