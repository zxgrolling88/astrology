package io.harc.jana.model;

public enum PayState {

    AppIdError(-4,"APPID_ERROR"), //appid错误
    TotalAmountError(-3,"TOTAL_AMOUNT_ERROR"),//金额错误
    FailSign(-2,"SIGN_VERIFY_FAILED"),//签名错误
    Wait(0,"WAIT_BUYER_PAY"),//等待支付
    Closed(1,"TRADE_CLOSED"),//未付款交易超时关闭或支付完成后全额退款
    Success(2,"TRADE_SUCCESS"),//交易成功可退款：成功状态
    Finished (3,"TRADE_FINISHED");//交易结束并不可退款：成功状态

    private int state;
    private String status;

    PayState(int state,String status){
        this.state=state;
        this.status=status;
    }

    public int getState() {
        return state;
    }

    public String getStatus() {
        return status;
    }

    public static boolean isSuccess(int state){
        return (state==Success.state || state==Finished.state);
    }

    @Override
    public String toString() {
        return   state+"";
    }
}
