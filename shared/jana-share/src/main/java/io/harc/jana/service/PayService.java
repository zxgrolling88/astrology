package io.harc.jana.service;

import io.harc.jana.model.Order;

public interface PayService {
    Order appPay(Order order) throws Exception;
}
