package io.harc.jana.model;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Role implements  Comparable<Role>,Serializable {

    long id;
    String roleName;
    String des;

    public Role(){}

    public Role(String roleName,String des){
        this.roleName=roleName;
        this.des=des;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    @Override
    public int compareTo(Role o) {
        return 0;
    }
}
