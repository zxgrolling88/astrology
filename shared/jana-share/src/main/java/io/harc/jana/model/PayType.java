package io.harc.jana.model;

public enum PayType {

    AliPay("AliPay"),//阿里支付
    WXPay("WXPay");//微信支付

    private String payType;

    PayType(String payType){
        this.payType=payType;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    @Override
    public String toString() {
        return   payType;
    }
}
