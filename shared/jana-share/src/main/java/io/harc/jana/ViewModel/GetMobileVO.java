package io.harc.jana.ViewModel;

public class GetMobileVO {
    String requestId;
    String code;
    String message;
    String mobile;

    public GetMobileVO(String requestId,String code,String message,String mobile){
        this.requestId=requestId;
        this.code=code;
        this.message=message;
        this.mobile=mobile;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
