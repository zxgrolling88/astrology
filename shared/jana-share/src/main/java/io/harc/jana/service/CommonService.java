package io.harc.jana.service;

import io.harc.jana.model.SearchArgs;

import java.util.List;
public interface CommonService<T> {

    T find(String key);
    int insert(T model);
    int update(T model);
    int delete(String key);
    List<T> search(int pageIndex, int pageSize, String orders,  SearchArgs... wheres);

}
