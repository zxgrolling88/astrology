package io.harc.jana.model;

import java.util.ArrayList;
import java.util.List;

public enum FixRole {
    Administrator("Administrator","超级管理员"),//所有权限
    SystemUser("SystemUser","系统用户"),//除了添加角色功能隐藏外，其它都可以操作。
    Operator("Operator","操作员"),//只能添加，修改数据，不能删除数据。
    Cooker("Cooker","厨师"),//控制展示界面
    Foodie("Foodie","吃货"),//普通用户
    Supplier("Supplier","供应商");//暂空

    FixRole(String name,String des){
        this.name=name;
        this.des=des;
    }

    String name;
    String des;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    static List<FixRole> srs=new ArrayList<>();
    public static List<FixRole> showRoles(){
        if(null==srs || srs.size()==0){
            srs.add(SystemUser);
            srs.add(Operator);
        }
        return  srs;
    }

}
