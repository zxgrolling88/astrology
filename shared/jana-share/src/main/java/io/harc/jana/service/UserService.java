package io.harc.jana.service;

import io.harc.jana.model.User;

import java.util.Date;
import java.util.List;
import java.util.Map;
public interface UserService<T> extends CommonService<T> {
   //public interface UserService<T> {
   int updateHungryDate(String userName, Date hungryDate);
   User findByPhone(String phone);
   int addRole(String operatorUid,String targetUid,String roles);

}
