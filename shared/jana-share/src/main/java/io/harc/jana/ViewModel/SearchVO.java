package io.harc.jana.ViewModel;

import io.harc.jana.model.SearchArgs;

import java.util.List;

public class SearchVO {

    int pageIndex;
    int pageSize;
    String orders;
    SearchArgs[] wheres;

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public String getOrders() {
        return orders;
    }

    public void setOrders(String orders) {
        this.orders = orders;
    }

    public SearchArgs[] getWheres() {
        return wheres;
    }

    public void setWheres(SearchArgs[] wheres) {
        this.wheres = wheres;
    }
}
