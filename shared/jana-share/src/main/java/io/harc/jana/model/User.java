package io.harc.jana.model;

import lombok.ToString;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@ToString
public class User implements Comparable<User>, Serializable {
    private Integer id;

    private String uid;

    private String userName;

    private String nickName;

    private String gender;

    private String roles;

    private String phone;

    private String avatar;

    private String features;

    private String style;

    private Float lat;

    private Float lon;

    private String country;

    private String province;

    private String city;

    private String district;

    private String quarter;

    private String building;

    private String floor;

    private String houseNo;

    private String workProvince;

    private String workCity;

    private String workDistrict;

    private String workPark;

    private String workBuilding;

    private String workFloor;

    private String workHouseNo;

    private String workLocCode;

    private String diviceId;

    private String mobileType;

    private Date hungryDate;

    private Date updateTime;

    private String token;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUid() {
        if(null==uid || uid.isEmpty())
            uid=UUID.randomUUID().toString();
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid == null ? null : uid.trim();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName == null ? null : nickName.trim();
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender == null ? null : gender.trim();
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles == null ? null : roles.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar == null ? null : avatar.trim();
    }

    public String getFeatures() {
        return features;
    }

    public void setFeatures(String features) {
        this.features = features == null ? null : features.trim();
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style == null ? null : style.trim();
    }

    public Float getLat() {
        return lat;
    }

    public void setLat(Float lat) {
        this.lat = lat;
    }

    public Float getLon() {
        return lon;
    }

    public void setLon(Float lon) {
        this.lon = lon;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country == null ? null : country.trim();
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district == null ? null : district.trim();
    }

    public String getQuarter() {
        return quarter;
    }

    public void setQuarter(String quarter) {
        this.quarter = quarter == null ? null : quarter.trim();
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building == null ? null : building.trim();
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor == null ? null : floor.trim();
    }

    public String getHouseNo() {
        return houseNo;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo == null ? null : houseNo.trim();
    }

    public String getWorkProvince() {
        return workProvince;
    }

    public void setWorkProvince(String workProvince) {
        this.workProvince = workProvince == null ? null : workProvince.trim();
    }

    public String getWorkCity() {
        return workCity;
    }

    public void setWorkCity(String workCity) {
        this.workCity = workCity == null ? null : workCity.trim();
    }

    public String getWorkDistrict() {
        return workDistrict;
    }

    public void setWorkDistrict(String workDistrict) {
        this.workDistrict = workDistrict == null ? null : workDistrict.trim();
    }

    public String getWorkPark() {
        return workPark;
    }

    public void setWorkPark(String workPark) {
        this.workPark = workPark == null ? null : workPark.trim();
    }

    public String getWorkBuilding() {
        return workBuilding;
    }

    public void setWorkBuilding(String workBuilding) {
        this.workBuilding = workBuilding == null ? null : workBuilding.trim();
    }

    public String getWorkFloor() {
        return workFloor;
    }

    public void setWorkFloor(String workFloor) {
        this.workFloor = workFloor == null ? null : workFloor.trim();
    }

    public String getWorkHouseNo() {
        return workHouseNo;
    }

    public void setWorkHouseNo(String workHouseNo) {
        this.workHouseNo = workHouseNo == null ? null : workHouseNo.trim();
    }

    public String getWorkLocCode() {
        return workLocCode;
    }

    public void setWorkLocCode(String workLocCode) {
        this.workLocCode = workLocCode == null ? null : workLocCode.trim();
    }

    public String getDiviceId() {
        return diviceId;
    }

    public void setDiviceId(String diviceId) {
        this.diviceId = diviceId == null ? null : diviceId.trim();
    }

    public String getMobileType() {
        return mobileType;
    }

    public void setMobileType(String mobileType) {
        this.mobileType = mobileType == null ? null : mobileType.trim();
    }

    public Date getHungryDate() {
        return hungryDate;
    }

    public void setHungryDate(Date hungryDate) {
        this.hungryDate = hungryDate;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token == null ? null : token.trim();
    }

    @Override
    public int compareTo(User o) {
        return 0;
    }
}