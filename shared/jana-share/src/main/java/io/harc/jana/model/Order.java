package io.harc.jana.model;

import java.util.Date;
import java.util.Map;
import java.util.UUID;

public class Order {

    long id;
    String uid;
    String orderId; //第三方支付的订单id.
    String payType;
    Double amount;
    String userUid;
    String userName;
    String nickName;
    Integer payState;
    String tradeStatus;
    Date payTime;
    String checkCode;
    String subject;
    String productCode;
    String des;
    Date updateTime;

    boolean tradeSuccess;

    boolean success;

    private String merchantOrderNo;
    private String outTradeNo;
    private String sellerId;
    private Double totalAmount;
    private String tradeNo;
    private String code;
    private String msg;
    private String subCode;
    private String subMsg;
    private String body;
    private Map<String, String> params;


    private Date notifyTime;
    private Date gmtCreate;

    private Date gmtPayment;
    private Date gmtRefund;
    private Date gmtClose;
    private String sellerEmail;
    private String outBizNo;


    private String sign;

    /**
     * 买家支付宝账号
     */
    private String buyerLogonId;

    /**
     * 实收金额，单位为元，两位小数。该金额为本笔交易，商户账户能够实际收到的金额
     */
    private String receiptAmount;

    /**
     * 交易中用户支付的可开具发票的金额，单位为元，两位小数。该金额代表该笔交易中可以给用户开具发票的金额
     */
    private String invoiceAmount;

    /**
     * 买家实付金额，单位为元，两位小数。该金额代表该笔交易买家实际支付的金额，不包含商户折扣等金额
     */
    private String buyerPayAmount;

    public String getTradeStatus() {
        return tradeStatus;
    }

    public void setTradeStatus(String tradeStatus) {
        this.tradeStatus = tradeStatus;
    }

    public String getBuyerLogonId() {
        return buyerLogonId;
    }

    public void setBuyerLogonId(String buyerLogonId) {
        this.buyerLogonId = buyerLogonId;
    }

    public String getReceiptAmount() {
        return receiptAmount;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public Date getNotifyTime() {
        return notifyTime;
    }

    public void setNotifyTime(Date notifyTime) {
        this.notifyTime = notifyTime;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public Date getGmtPayment() {
        return gmtPayment;
    }

    public void setGmtPayment(Date gmtPayment) {
        this.gmtPayment = gmtPayment;
    }

    public Date getGmtRefund() {
        return gmtRefund;
    }

    public void setGmtRefund(Date gmtRefund) {
        this.gmtRefund = gmtRefund;
    }

    public Date getGmtClose() {
        return gmtClose;
    }

    public void setGmtClose(Date gmtClose) {
        this.gmtClose = gmtClose;
    }

    public String getSellerEmail() {
        return sellerEmail;
    }

    public void setSellerEmail(String sellerEmail) {
        this.sellerEmail = sellerEmail;
    }

    public String getOutBizNo() {
        return outBizNo;
    }

    public void setOutBizNo(String outBizNo) {
        this.outBizNo = outBizNo;
    }

    public void setReceiptAmount(String receiptAmount) {
        this.receiptAmount = receiptAmount;
    }

    public String getInvoiceAmount() {
        return invoiceAmount;
    }

    public void setInvoiceAmount(String invoiceAmount) {
        this.invoiceAmount = invoiceAmount;
    }

    public String getBuyerPayAmount() {
        return buyerPayAmount;
    }

    public void setBuyerPayAmount(String buyerPayAmount) {
        this.buyerPayAmount = buyerPayAmount;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUid() {
        if(null==uid || uid.isEmpty()) {
            uid = UUID.randomUUID().toString();
            outTradeNo=uid;
        }
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getDes() {
        if(null==des)
            des=subject;
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public boolean isTradeSuccess() {
        return tradeSuccess;
    }

    public void setTradeSuccess(boolean tradeSuccess) {
        this.tradeSuccess = tradeSuccess;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getSubCode() {
        return subCode;
    }

    public void setSubCode(String subCode) {
        this.subCode = subCode;
    }

    public String getSubMsg() {
        return subMsg;
    }

    public void setSubMsg(String subMsg) {
        this.subMsg = subMsg;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getUserUid() {
        return userUid;
    }

    public void setUserUid(String userUid) {
        this.userUid = userUid;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Integer getPayState() {
        return payState;
    }

    public void setPayState(Integer payState) {
        this.payState = payState;
    }

    public Date getPayTime() {
        return payTime;
    }

    public void setPayTime(Date payTime) {
        this.payTime = payTime;
    }

    public String getCheckCode() {
        return checkCode;
    }

    public void setCheckCode(String checkCode) {
        this.checkCode = checkCode;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Date getUpdateTime() {
        if(null==updateTime)
            updateTime=new Date();
        return updateTime;
    }

    public String getMerchantOrderNo() {
        return merchantOrderNo;
    }

    public void setMerchantOrderNo(String merchantOrderNo) {
        this.merchantOrderNo = merchantOrderNo;
    }

    public String getOutTradeNo() {
        return outTradeNo;
    }

    public void setOutTradeNo(String outTradeNo) {
        this.outTradeNo = outTradeNo;
    }

    public String getSellerId() {
        return sellerId;
    }

    public void setSellerId(String sellerId) {
        this.sellerId = sellerId;
    }

    public Double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(Double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getTradeNo() {
        return tradeNo;
    }

    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }







    @Override
    public String toString(){
        return "id "+			id+
                "  uid "+			uid+
                "  orderId "+ 		orderId+
                "  payType "+		payType+
                "  amount "+		amount+
                "  payState "+		payState+
                "  userUid "+		userUid+
                "  userName "+		userName+
                "  nickName "+		nickName+
                "  payState "+		payState+
                "  payTime "+		payTime+
                "  checkCode "+		checkCode+
                "  subject "+		subject+
                "  productCode "+		productCode+
                "  des "+			des+
                "  updateTime "+		updateTime+
                "  success "+		success+
                "  merchantOrderNo"+	merchantOrderNo+
                "  outTradeNo "+		outTradeNo+
                "  sellerId "+		sellerId+
                "  totalAmount "+	totalAmount+
                "  tradeNo "+		tradeNo+
                "  code "+		code+
                "  msg "+		msg+
                "  subCode "+		subCode+
                "  subMsg "+		subMsg+
                "  body "+		body;


    }

}
