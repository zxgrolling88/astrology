package io.harc.alipay;

import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradeAppPayModel;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.request.AlipayTradeAppPayRequest;
import com.alipay.api.request.AlipayTradeQueryRequest;
import com.alipay.api.response.AlipayTradeAppPayResponse;
import com.alipay.api.response.AlipayTradeQueryResponse;
import io.harc.common.DateUtil;
import io.harc.jana.model.Order;
import io.harc.jana.model.PayState;

import java.util.Map;

public class AlipayService {


    static AlipayClient client = new DefaultAlipayClient(PayConfig.GatewayUrl, PayConfig.AppId, PayConfig.PrivateKey, PayConfig.Format, PayConfig.Charset,PayConfig.PublicKey,PayConfig.SignType);

    /**
     * app pay
     * @param order
     * @return
     * @throws Exception
     */
    public Order charge(Order order) throws Exception {
        AlipayTradeAppPayRequest req = new AlipayTradeAppPayRequest();
        req.setNotifyUrl(PayConfig.NotifyUrl);
        req.setReturnUrl(PayConfig.ReturnUrl);
        AlipayTradeAppPayModel model = fromOrder(order);
        req.setBizModel(model);
        AlipayTradeAppPayResponse res = client.sdkExecute(req);
        toOrder(order, res);
        System.out.println(order.toString());
        return order;
    }

    public void aliNotify(Order order,Map<String, String> params)  throws  Exception {
       verify(order, params);
}

    private void verify(Order order,Map<String, String> params) throws  Exception{
        //调用SDK验证签名
        String alipayPublicKey = PayConfig.PublicKey;
        String charset = PayConfig.Charset;
        String signType = PayConfig.SignType;
        String tradeStatus = params.get("trade_status");
        order.setSign(params.get("sign"));
        boolean signVerified = AlipaySignature.rsaCheckV1(params, alipayPublicKey, charset, signType);
        if (signVerified) {
            String appId = params.get("app_id");//支付宝分配给开发者的应用Id
            String notifyTime = params.get("notify_time");//通知时间:yyyy-MM-dd HH:mm:ss
            String gmtCreate = params.get("gmt_create");//交易创建时间:yyyy-MM-dd HH:mm:ss
            String gmtPayment = params.get("gmt_payment");//交易付款时间
            String gmtRefund = params.get("gmt_refund");//交易退款时间
            String gmtClose = params.get("gmt_close");//交易结束时间
            String tradeNo = params.get("trade_no");//支付宝的交易号
            String outTradeNo = params.get("out_trade_no");//获取商户之前传给支付宝的订单号（商户系统的唯一订单号）
            String outBizNo = params.get("out_biz_no");//商户业务号(商户业务ID，主要是退款通知中返回退款申请的流水号)
            String buyerLogonId = params.get("buyer_logon_id");//买家支付宝账号
            String sellerId = params.get("seller_id");//卖家支付宝用户号
            String sellerEmail = params.get("seller_email");//卖家支付宝账号
            Double totalAmount = Double.parseDouble(params.get("total_amount"));//订单金额:本次交易支付的订单金额，单位为人民币（元）
            String receiptAmount = params.get("receipt_amount");//实收金额:商家在交易中实际收到的款项，单位为元
            String invoiceAmount = params.get("invoice_amount");//开票金额:用户在交易中支付的可开发票的金额
            String buyerPayAmount = params.get("buyer_pay_amount");//付款金额:用户在交易中支付的金额
            if (PayConfig.AppId.equals(appId)) {
                //修改数据库支付宝订单表(因为要保存每次支付宝返回的信息到数据库里，以便以后查证)
                setStatus(order,tradeStatus);
                order.setNotifyTime(DateUtil.parse(notifyTime));
                order.setGmtCreate(DateUtil.parse(gmtCreate));
                order.setGmtPayment(DateUtil.parse(gmtPayment));
                order.setGmtRefund(DateUtil.parse(gmtRefund));
                order.setGmtClose(DateUtil.parse(gmtClose));
                order.setSellerEmail(sellerEmail);
                order.setOutBizNo(outBizNo);
                order.setTradeNo(tradeNo);
                order.setBuyerLogonId(buyerLogonId);
                order.setSellerId(sellerId);
                order.setTotalAmount(totalAmount);
                order.setReceiptAmount(receiptAmount);
                order.setInvoiceAmount(invoiceAmount);
                order.setBuyerPayAmount(buyerPayAmount);
            }
            else{
                order.setPayState(PayState.AppIdError.getState());
                order.setTradeSuccess(false);
            }
        }
        else{
            order.setPayState(PayState.FailSign.getState());
            order.setTradeSuccess(false);
        }

}

   void setStatus(Order order,String tradeStatus){
       order.setTradeStatus(tradeStatus);
       switch (tradeStatus) // 判断交易结果
       {
           case "TRADE_FINISHED": // 交易结束并不可退款
               order.setPayState(PayState.Finished.getState());
               order.setTradeSuccess(true);
               break;
           case "TRADE_SUCCESS": // 交易支付成功
               order.setPayState(PayState.Success.getState());
               order.setTradeSuccess(true);
               break;
           case "TRADE_CLOSED": // 未付款交易超时关闭或支付完成后全额退款
               order.setPayState(PayState.Closed.getState());
               order.setTradeSuccess(false);
               break;
           case "WAIT_BUYER_PAY": // 交易创建并等待买家付款
               order.setPayState(PayState.Wait.getState());
               order.setTradeSuccess(false);
               break;
           default:
               break;
       }

   }

    public  int checkAlipay(Order order) throws Exception{

            AlipayTradeQueryRequest alipayTradeQueryRequest = new AlipayTradeQueryRequest();
            alipayTradeQueryRequest.setBizContent("{" +
                    "\"out_trade_no\":\""+order.getUid()+"\"" +
                    "}");
            AlipayTradeQueryResponse res = client.execute(alipayTradeQueryRequest);
            if(res.isSuccess()){

                //修改数据库支付宝订单表
                order.setTradeNo(res.getTradeNo());
                order.setBuyerLogonId(res.getBuyerLogonId());
                order.setTotalAmount(Double.parseDouble(res.getTotalAmount()));
                order.setReceiptAmount(res.getReceiptAmount());
                order.setInvoiceAmount(res.getInvoiceAmount());
                order.setBuyerPayAmount(res.getBuyerPayAmount());
            }
            else{
                order.setMsg(res.getMsg());
                order.setCode(res.getCode());
                order.setSubMsg(res.getSubMsg());
                order.setSubCode(res.getSubCode());
            }
            setStatus(order,res.getTradeStatus());
        return order.getPayState();
    }


    private AlipayTradeAppPayModel fromOrder(Order order){
        AlipayTradeAppPayModel model=new AlipayTradeAppPayModel();
        model.setSubject(order.getSubject());
        model.setOutTradeNo(order.getUid());
        model.setTotalAmount(order.getAmount().toString());
        //model.setTimeoutExpress("30m");
        model.setProductCode(PayConfig.ProductCode);
        model.setBody(order.getDes());
        return model;
    }

    private void toOrder(Order order,AlipayTradeAppPayResponse res){
         order.setMerchantOrderNo(res.getMerchantOrderNo());
         order.setOutTradeNo(res.getOutTradeNo());
         order.setSellerId(res.getSellerId());
         try {
             order.setTotalAmount(Double.parseDouble(res.getTotalAmount()));
             order.setAmount(Double.parseDouble(res.getTotalAmount()));
         }
         catch(Exception e){
             e.printStackTrace();
         }
         order.setTradeNo(res.getTradeNo());
         order.setCode(res.getCode());
         order.setMsg(res.getMsg());
         order.setSubCode(res.getSubCode());
         order.setSubMsg(res.getSubMsg());
         order.setBody(res.getBody());
         order.setSuccess(res.isSuccess());
    }


}
