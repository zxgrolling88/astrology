package io.harc.jana;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(value = "io.harc.jana.controller")
@ComponentScan(basePackages={"io.harc.jana.*"})
@MapperScan(basePackages = "io.harc.jana.dao")
public class JanaApplication {

    public static void main(String[] args) {
        SpringApplication.run(JanaApplication.class, args);
    }

}

//@EnableFeignClients(basePackages = "com.example.zxg.goods.feign")
//@SpringBootApplication
//@EnableDiscoveryClient
//public class GoodsApplication {
//
//    public static void main(String[] args) {
//        SpringApplication.run(GoodsApplication.class, args);
//    }
//
//}