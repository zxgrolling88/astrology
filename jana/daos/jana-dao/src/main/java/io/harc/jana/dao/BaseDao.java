package io.harc.jana.dao;

import io.harc.jana.model.SearchArgs;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BaseDao<T>  {
    T find(long id);
    T find(String key);
    int insert(T model);
    int update(T  model);
    int delete(long id);
    int delete(String key);
    List<T> search(@Param("pageIndex")int pageIndex,@Param("pageSize")int pageSize,@Param("orders")String orders,@Param("wheres")SearchArgs... wheres);
}
