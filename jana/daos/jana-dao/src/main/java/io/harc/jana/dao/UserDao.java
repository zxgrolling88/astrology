package io.harc.jana.dao;


import io.harc.jana.model.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface UserDao<T> extends BaseDao<T>{

    int updateHungryDate(String uid, Date hungryDate);

    User findByPhone(String phone);

    int addRole(@Param("operatorUid") String operatorUid, @Param("targetUid") String targetUid, @Param("roles")String roles);

}