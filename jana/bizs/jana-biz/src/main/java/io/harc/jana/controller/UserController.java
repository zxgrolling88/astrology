package io.harc.jana.controller;

import io.harc.auth.annotation.PassToken;
import io.harc.common.Result;
import io.harc.jana.ViewModel.GetMobileVO;
import io.harc.jana.ViewModel.SearchVO;
import io.harc.jana.fcd.UserFcd;
import io.harc.jana.model.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = {"/user"})
public class UserController {

    @Autowired
    UserFcd fcd;

    @GetMapping(value="/find")
    public Result<User> find(String uid){
        return  fcd.find(uid);
    }

    @PostMapping(value="/insert")
    public Result<Integer> insert(@RequestBody User user){
        return fcd.insert(user);
    }

    @PostMapping(value="/update")
    public Result<Integer> update(@RequestBody User user){
        log.info(user.toString());
        return fcd.update(user);
    }

    @PostMapping(value="/delete")
    public Result<Integer> delete(String uid){
        return fcd.delete(uid);
    }

    @PostMapping(value="/search")
    public Result<List<User>> search(@RequestBody SearchVO svo){
        return fcd.search(svo.getPageIndex(),svo.getPageSize(),svo.getOrders(),svo.getWheres());
    }

    @PassToken
    @PostMapping(value="/regist")
    public Result<User> regist(@RequestBody User user){
        log.info(user.toString());
        log.info(user.getUserName());
        return fcd.regist(user);
    }

    @PassToken
    @GetMapping(value="/login")
    public Result<User> login(@RequestParam String phone){
        return fcd.login(phone);
    }

    @PassToken
    @PostMapping(value="/getMobile")
    public Result<GetMobileVO> getMobile(@RequestBody String token){
        return fcd.getMobile(token);
    }

}
