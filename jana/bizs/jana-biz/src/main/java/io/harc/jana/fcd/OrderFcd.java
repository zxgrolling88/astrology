package io.harc.jana.fcd;
import io.harc.common.Encrypt;
import io.harc.common.Result;
import io.harc.jana.model.Order;
import io.harc.jana.model.PayState;
import io.harc.jana.service.CommonService;
import io.harc.jana.service.OrderService;
import io.harc.alipay.AlipayService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Component
public class OrderFcd extends BaseFcd<Order> {
    @Autowired
    OrderService<Order> modelService;

    AlipayService alipayService;

    public OrderFcd() {
        if (null == alipayService)
            alipayService = new AlipayService();
    }

    @Override
    protected CommonService loadService() {
        return modelService;
    }

    public Result<Order> charge(Order order) {
        Result<Order> result = new Result<>();
        try {
            order.setPayState(PayState.Wait.getState());
            order = alipayService.charge(order);
            int rtn = -1;
            System.out.println(order.toString());
            if (order.isSuccess()) {
//                String checkCode=Encrypt.SHA1(order.getUid()+order.getTotalAmount()+order.getPayState());
//                order.setCheckCode(checkCode);
                rtn=modelService.insert(order);
            }
            return new Result(order);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result<>(e);
        }
    }

    public Result<Integer> checkAlipay(String uid){
        try {
            Order order=modelService.find(uid);
            int state=alipayService.checkAlipay(order);
            String checkCode=Encrypt.SHA1(order.getUid()+order.getTotalAmount()+order.getPayState());
            order.setCheckCode(checkCode);
            modelService.update(order);
            return new Result<Integer>(state);
        }
        catch (Exception e){
            e.printStackTrace();
            return new Result<>(e);
        }
    }


    public String alipayNotify(HttpServletRequest request, HttpServletResponse response) {
        Map<String, String[]> aliParams = request.getParameterMap();
        //用以存放转化后的参数集合
        try {
            Map<String, String> params = new HashMap<String, String>();
            for (Iterator<String> iter = aliParams.keySet().iterator(); iter.hasNext(); ) {
                String key = iter.next();
                String[] values = aliParams.get(key);
                String valueStr = "";
                for (int i = 0; i < values.length; i++) {
                    valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
                }
                // 乱码解决，这段代码在出现乱码时使用。如果mysign和sign不相等也可以使用这段代码转化
                //valueStr = new String(valueStr.getBytes("ISO-8859-1"), "UTF-8");
                params.put(key, valueStr);
            }
            System.out.println("==返回参数集合：" + params);
            String uid=params.get("out_trade_no");
            Order order=modelService.find(uid);
            if(null==order){
                System.out.println("没有找订单：" + uid);
                return "fail";
            }
            alipayService.aliNotify(order,params);
            boolean success= PayState.isSuccess(order.getPayState());
            System.out.println("订单状态：" + success+ " "+ order.getPayState() + "" +order.getTradeStatus());
            if(success){
                String checkCode=Encrypt.SHA1(order.getUid()+order.getTotalAmount()+order.getPayState());
                order.setCheckCode(checkCode);
                modelService.update(order);
                return "success";
            }
            return "fail";
        } catch (Exception e) {
            e.printStackTrace();
            return e.getMessage();
        }
    }


}
