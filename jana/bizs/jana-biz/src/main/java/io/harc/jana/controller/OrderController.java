package io.harc.jana.controller;

import io.harc.auth.annotation.PassToken;
import io.harc.common.Result;
import io.harc.jana.ViewModel.SearchVO;
import io.harc.jana.fcd.OrderFcd;
import io.harc.jana.model.Order;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = {"/order"})
public class OrderController {

    @Autowired
    OrderFcd fcd;

    @GetMapping(value="/find")
    public Result<Order> find(String uid){
        return  fcd.find(uid);
    }

    @PostMapping(value="/insert")
    public Result<Integer> insert(@RequestBody Order order){
        return fcd.insert(order);
    }


    @PostMapping(value="/search")
    public Result<List<Order>> search(@RequestBody SearchVO svo){
        return fcd.search(svo.getPageIndex(),svo.getPageSize(),svo.getOrders(),svo.getWheres());
    }


    @PostMapping(value="/charge")
    public Result<Order> charge(@RequestBody Order order){
        return fcd.charge(order);
    }

    @PassToken
    @PostMapping(value="alipayNotify")
    public String alipayNotify(HttpServletRequest request, HttpServletResponse response) {
        log.info("alipayNotify");
        return fcd.alipayNotify(request,response);
    }

    @PostMapping(value="checkAlipay")
    public Result<Integer> checkAlipay(@RequestBody String uid) {
        return fcd.checkAlipay(uid);
    }

}
