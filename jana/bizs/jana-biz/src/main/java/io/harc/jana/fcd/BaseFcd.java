package io.harc.jana.fcd;

import io.harc.common.Result;
import io.harc.jana.model.SearchArgs;
import io.harc.jana.service.CommonService;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class BaseFcd<T> {

    protected CommonService<T> commonService;

    protected CommonService<T> loadService(){
        return commonService;
    }

    public Result<T> find(String uid){
        try {
            T o=loadService().find(uid);
            if(null!=o)
                return new Result(o);
            return new Result(new Exception("没有找到订单！"));
        }
        catch (Exception e){
            return  new Result(e);
        }
    }

    public <TR> Result<TR>  insert(T model){
        try{
            return new Result(loadService().insert(model));}
        catch (Exception e){
            return  new Result(e);
        }
    }

    public Result<Integer> update(T model){
        try{
            return new Result(loadService().update(model));
        }
        catch (Exception e){
            return  new Result(e);
        }
    }

    public Result<Integer> delete(String uid){
        try{
            return new Result(loadService().delete(uid));
        }
        catch (Exception e){
            return  new Result(e);
        }
    }

    public Result<List<T>> search(int pageIndex,int pageSize,String orders, SearchArgs... wheres){
        try{
            return new Result(loadService().search(pageIndex,pageSize,orders,wheres));
        }
        catch (Exception e){
            return  new Result(e);
        }
    }

    public Result<List<T>> search(int pageIndex,int pageSize,String orders,SearchArgs where){
        try{
            return new Result(loadService().search(pageIndex,pageSize,orders,where));
        }
        catch (Exception e){
            return  new Result(e);
        }
    }

}
