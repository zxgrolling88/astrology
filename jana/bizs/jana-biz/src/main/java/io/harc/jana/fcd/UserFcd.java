package io.harc.jana.fcd;

import com.aliyuncs.dypnsapi.model.v20170525.GetMobileResponse;
import io.harc.auth.TokenService;
import io.harc.common.Encrypt;
import io.harc.common.Result;
import io.harc.jana.Configs;
import io.harc.jana.ViewModel.GetMobileVO;
import io.harc.jana.model.FixRole;
import io.harc.jana.model.User;
import io.harc.jana.service.CommonService;
import io.harc.jana.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

@Slf4j
@Component
public class UserFcd  extends BaseFcd<User> {
    @Autowired
    UserService<User> modelService;

    @Override
    protected CommonService loadService(){
        return modelService;
    }

    public Result<User> regist(User user){
        try{
            user.setUid(UUID.randomUUID().toString());
            Date current_date = new Date();
            SimpleDateFormat SimpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String dateStr = SimpleDateFormat.format(current_date.getTime());
            log.info("Datatime: " + dateStr);
            user.setHungryDate(current_date);
            user.setUpdateTime(current_date);
            String loc=user.getWorkProvince()+user.getWorkCity()+user.getWorkDistrict()+user.getWorkPark()+user.getWorkBuilding()+user.getWorkFloor()+user.getWorkHouseNo();
            user.setWorkLocCode(Encrypt.SHA1(loc));
            // 为新注册用户分配新的TOKEN
            user.setToken(TokenService.createToken(Configs.JWTSecret,user.getUid()));
            if(null!=user.getRoles())
                user.setRoles(FixRole.Foodie.getName());

            int rtn=modelService.insert(user);
            if(rtn>0)
                return new Result(user);
            return new Result(-2,"注册失败!");
        }
        catch (Exception e){
            return  new Result(e);
        }
    }

    public  Result<User> login(String  phone){
        try{
            User user=modelService.findByPhone(phone);
            if(null!=user) {
                // 每次登陆成功返回新的TOKEN
                user.setToken(TokenService.createToken(Configs.JWTSecret,user.getUid()));
                return new Result(user);
            }
            return new Result(-2,"没有找到用户!");
        }
        catch (Exception e){
            return  new Result(e);
        }
    }

    public Result<GetMobileVO> getMobile(String token){
        try {
            GetMobileResponse res = AliyunTool.getMobile(token);
            if (null != res) {
                GetMobileVO gmv = new GetMobileVO(res.getRequestId(), res.getCode(), res.getMessage(), res.getGetMobileResultDTO().getMobile());
                return new Result<>(gmv);
            }
            return new Result<>(-1, "返回为空，取号失败!");
        }
        catch (Exception e){
            return new Result<>(e);
        }
    }

    public Result<Integer> addRole(String operatorUid,String targetUid,String roles){
        User user=modelService.find(operatorUid);
        if(null!=user && user.getRoles().indexOf(FixRole.Administrator.getName())>-1){
            return new Result<>(modelService.addRole(operatorUid,targetUid,roles));
        }
        return new Result<>(-2,"用户 "+operatorUid+" 没有权限添加角色！");
    }


}
