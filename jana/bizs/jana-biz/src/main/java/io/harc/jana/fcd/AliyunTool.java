package io.harc.jana.fcd;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dypnsapi.model.v20170525.GetMobileRequest;
import com.aliyuncs.dypnsapi.model.v20170525.GetMobileResponse;
import com.aliyuncs.profile.DefaultProfile;
import io.harc.jana.Configs;

public class AliyunTool {

    static IAcsClient iac=null;
    public static IAcsClient getClient(){
        if(null==iac) {
            DefaultProfile profile = DefaultProfile.getProfile(Configs.AliyunRegionId, Configs.AliyunRAMAccessKey, Configs.AliyunRAMSecretKey);
            iac = new DefaultAcsClient(profile);
        }
        return  iac;
    }

    public static GetMobileResponse getMobile(String token) throws Exception{
        IAcsClient client=getClient();
        GetMobileRequest request = new GetMobileRequest();
        request.setAccessToken(token);
        return client.getAcsResponse(request);
    }

}
