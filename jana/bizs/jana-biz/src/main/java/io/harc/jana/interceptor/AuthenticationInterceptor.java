package io.harc.jana.interceptor;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import io.harc.auth.annotation.PassToken;
import io.harc.common.DateUtil;
import io.harc.common.Encrypt;
import io.harc.jana.Configs;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.method.HandlerMethod;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.List;

@Slf4j
public class AuthenticationInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object object) throws Exception {
        String token = httpServletRequest.getHeader("token");   // 从 http 请求头中取出 token
        // 如果不是映射到方法直接通过
        if(!(object instanceof HandlerMethod)){
            //log.info("HandlerInterceptor not HandlerMethod");
            return true;
        }
        HandlerMethod handlerMethod=(HandlerMethod)object;
        Method method=handlerMethod.getMethod();
        //检查是否有passtoken注释，有则跳过认证
        if (method.isAnnotationPresent(PassToken.class)) {
            PassToken passToken = method.getAnnotation(PassToken.class);
            if (passToken.required()) {
                //log.info("HandlerInterceptor skip passToken");
                return true;
            }
        }
        // 执行认证
        if (token == null) {
            throw new RuntimeException("无token，请重新登录");
        }
        try {
            List<String> ads=JWT.decode(token).getAudience();
            String userId = ads.get(0); //可缓存判断用户是否存在
            String utc=httpServletRequest.getHeader("ts");
            String  hash = httpServletRequest.getHeader("hash");
            long ts=DateUtil.getNowUTCDate().getTime(); //可以比较当前时间设置一个和超时时间相当的过期时间.s
            //if(Encrypt.SHA1(utc+token).equals(hash)) {
            //if(Long.parseLong(utc)>ts && Encrypt.SHA1(utc+token).equals(hash)) {
                JWTVerifier jwtVerifier = JWT.require(Algorithm.HMAC256(Configs.JWTSecret)).build();
                jwtVerifier.verify(token);
                //log.info("verify token " + token);
                return true;
            //}
        } catch (JWTVerificationException e) {
            throw new RuntimeException("401");
        }
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }
    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
