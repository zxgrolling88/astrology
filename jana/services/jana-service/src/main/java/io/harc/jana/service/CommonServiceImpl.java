package io.harc.jana.service;

import io.harc.jana.dao.BaseDao;
import io.harc.jana.model.SearchArgs;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommonServiceImpl<T> implements CommonService<T> {

    protected  BaseDao<T> loadDao(){
        return  null;
    }

    public CommonServiceImpl(){}

    @Override
    public T find(String key) {
        return loadDao().find(key);
    }

    @Override
    public int insert(T model) {
        return loadDao().insert(model);
    }

    @Override
    public int update(T model) {
        return loadDao().update(model);
    }

    @Override
    public int delete(String key) {
        return loadDao().delete(key);
    }

    @Override
    public List<T> search(int pageIndex, int pageSize, String orderBy, SearchArgs... wheres) {
        return loadDao().search(pageIndex,pageSize,orderBy,wheres);
    }

}
