package io.harc.jana.service;

import io.harc.jana.dao.OrderDao;
import io.harc.jana.dao.BaseDao;
import io.harc.jana.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class OrderServiceImpl extends CommonServiceImpl<Order> implements OrderService<Order> {


    @Autowired
    protected OrderDao<Order> dao;

    @Override
    protected BaseDao<Order> loadDao(){
        return  dao;
    }

}