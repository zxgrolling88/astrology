package io.harc.jana.service;

import io.harc.jana.dao.BaseDao;
import io.harc.jana.dao.UserDao;
import io.harc.jana.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class UserServiceImpl extends CommonServiceImpl<User> implements UserService<User> {


    @Autowired
    protected    UserDao<User> dao;

    @Override
    protected BaseDao<User> loadDao(){
        return  dao;
    }

    @Override
    public int updateHungryDate(String uid, Date hungryDate){
        return  dao.updateHungryDate(uid,hungryDate);
    }

    @Override
    public User findByPhone(String phone) {
        return dao.findByPhone(phone);
    }

    @Override
    public int addRole(String operatorUid,String targetUid,String roles) {
        return dao.addRole(operatorUid,targetUid,roles);
    }

}
